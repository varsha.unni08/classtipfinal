package com.classtips.liju.classtips.Database.DataSource;

import com.classtips.liju.classtips.Database.ModelDB.classtipRecent;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/5/2018.
 */

public interface IclasstipRecentDataSource {
    Flowable<List<classtipRecent>> getRecentVideos();
    public List<classtipRecent> getRecentVideosById(int topicId);
    int isExist(int videoId);
    int countVideos();
    void  emptyVideos();
    void insertVideos(classtipRecent...classtipRecents);
    void updateVideos(classtipRecent...classtipRecents);
    void deleteVideos(classtipRecent...classtipRecents);
}
