package com.classtips.liju.classtips.Database.Local;

import com.classtips.liju.classtips.Database.DataSource.IclasstipQuestionYearDataSource;
import com.classtips.liju.classtips.Database.ModelDB.classtipQuestionYear;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/27/2018.
 */

public class classtipQuestionYearDataSource implements IclasstipQuestionYearDataSource {

    private classtipQuestionYearDAO questionYearDAO;
    private static classtipQuestionYearDataSource instance;

    public classtipQuestionYearDataSource(classtipQuestionYearDAO questionYearDAO) {
        this.questionYearDAO = questionYearDAO;
    }

    public static classtipQuestionYearDataSource getInstance(classtipQuestionYearDAO questionYearDAO){
        if (instance==null)
            instance=new classtipQuestionYearDataSource(questionYearDAO);
        return instance;
    }

    @Override
    public Flowable<List<classtipQuestionYear>> getQuestions() {
        return questionYearDAO.getQuestions();
    }

    @Override
    public Flowable<List<classtipQuestionYear>> getQuestionsById(int examID) {
        return questionYearDAO.getQuestionsById(examID);
    }

    @Override
    public int isExist(int questionId) {
        return questionYearDAO.isExist(questionId);
    }

    @Override
    public int countQuestions() {
        return questionYearDAO.countQuestions();
    }

    @Override
    public void insertQuestions(classtipQuestionYear... classtipQuestions) {
        questionYearDAO.insertQuestions(classtipQuestions);
    }

    @Override
    public void updateQuestions(classtipQuestionYear... classtipQuestionYears) {
        questionYearDAO.updateQuestions(classtipQuestionYears);
    }
}
