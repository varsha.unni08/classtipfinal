package com.classtips.liju.classtips.Database.Local;

import com.classtips.liju.classtips.Database.DataSource.IclasstipUserDataSource;
import com.classtips.liju.classtips.Database.ModelDB.classtipUser;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/12/2018.
 */

public class classtipUserDataSource implements IclasstipUserDataSource {
    private classtipUserDAO userDAO;
    private static classtipUserDataSource instance;

    public classtipUserDataSource(classtipUserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public static classtipUserDataSource getInstance(classtipUserDAO userDAO){
        if (instance==null)
            instance=new classtipUserDataSource(userDAO);
        return instance;
    }
    @Override
    public Flowable<List<classtipUser>> getUsers() {
        return userDAO.getUsers();
    }

    @Override
    public Flowable<List<classtipUser>> getUsersById(int courseId) {
        return userDAO.getUsersById(courseId);
    }

    @Override
    public int isExist(int phone) {
        return userDAO.isExist(phone);
    }

    @Override
    public int isUpdated(String flag, String phone) {
        return userDAO.isUpdated(flag,phone);
    }

    @Override
    public void updateUser(String flag, String phone) {
        userDAO.updateUser(flag, phone);
    }

    @Override
    public int countUsers() {
        return userDAO.countUsers();
    }

    @Override
    public void insertUsers(classtipUser... classtipUsers) {
        userDAO.insertUsers(classtipUsers);
    }

    @Override
    public void updateUserDate(String date, String phone) {
        userDAO.updateUserDate(date, phone);
    }

    @Override
    public void updateExpiryFlag(String flag, String phone) {
        userDAO.updateExpiryFlag(flag, phone);
    }
}
