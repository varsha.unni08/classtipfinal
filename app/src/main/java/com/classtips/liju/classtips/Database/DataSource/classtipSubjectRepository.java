package com.classtips.liju.classtips.Database.DataSource;

import com.classtips.liju.classtips.Database.ModelDB.classtipSubject;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/7/2018.
 */

public class classtipSubjectRepository implements IclasstipSubjectDatasource {
    private IclasstipSubjectDatasource iclasstipSubjectDatasource;

    public classtipSubjectRepository(IclasstipSubjectDatasource iclasstipSubjectDatasource) {
        this.iclasstipSubjectDatasource = iclasstipSubjectDatasource;
    }

    private static classtipSubjectRepository instance;

    public static classtipSubjectRepository getInstance(IclasstipSubjectDatasource iclasstipSubjectDatasource){
        if (instance==null)
            instance=new classtipSubjectRepository(iclasstipSubjectDatasource);
        return instance;
    }

    @Override
    public Flowable<List<classtipSubject>> getSubjects() {
        return iclasstipSubjectDatasource.getSubjects();
    }

    @Override
    public Flowable<List<classtipSubject>> getSubjectsById(int courseId) {
        return iclasstipSubjectDatasource.getSubjectsById(courseId);
    }

    @Override
    public int isExist(int subId) {
        return iclasstipSubjectDatasource.isExist(subId);
    }

    @Override
    public int countSubjects() {
        return iclasstipSubjectDatasource.countSubjects();
    }

    @Override
    public void emptySubjects() {
        iclasstipSubjectDatasource.emptySubjects();
    }

    @Override
    public void insertSubjects(classtipSubject... classtipSubjects) {
        iclasstipSubjectDatasource.insertSubjects(classtipSubjects);
    }

    @Override
    public void updateSubjects(classtipSubject... classtipSubjects) {
        iclasstipSubjectDatasource.updateSubjects(classtipSubjects);
    }

    @Override
    public void deleteSubjects(classtipSubject... classtipSubjects) {
        iclasstipSubjectDatasource.deleteSubjects(classtipSubjects);
    }
}
