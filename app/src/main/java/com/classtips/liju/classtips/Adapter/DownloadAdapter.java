package com.classtips.liju.classtips.Adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.classtips.liju.classtips.CartActivity;
import com.classtips.liju.classtips.Database.ModelDB.classtipDownload;
import com.classtips.liju.classtips.Database.ModelDB.classtipSubject;
import com.classtips.liju.classtips.Downloading.DownloadTask;
import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.Model.CheckPurchaseResponse;
import com.classtips.liju.classtips.Model.DownloadList;
import com.classtips.liju.classtips.PurchaseActivity;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by User on 9/21/2018.
 */

public class DownloadAdapter extends RecyclerView.Adapter<DownloadAdapter.DownloadViewHolder> {

    Context context;
    IClassTipAPI mService;
    List<DownloadList> downloadLists;
    String phone;

    public DownloadAdapter(Context context, List<DownloadList> downloadLists) {
        this.context = context;
        this.downloadLists = downloadLists;
    }

    @Override
    public DownloadViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View iteView= LayoutInflater.from(context).inflate(R.layout.download_item_layout,parent,false);
        return new DownloadAdapter.DownloadViewHolder(iteView);
    }

    @Override
    public void onBindViewHolder(final DownloadViewHolder holder, final int position) {
        holder.txt_subject_name.setText(downloadLists.get(position).file_name);

        if (Common.downloadRepository.isFlgExist("Y",Integer.parseInt(downloadLists.get(position).download_id))==1){
            holder.text_download.setText("Already Downloaded" +"\n"+
                    "Click to download again");
            holder.text_download.setBackgroundResource(R.drawable.text_round_red);
        }

        holder.text_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.text_download.setEnabled(false);
                    Common.currentDownload=downloadLists.get(position);
                    SharedPreferences pref = context.getSharedPreferences("MyPref", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();

                    try {
                        phone=Common.currentUser.getPhone();
                    }catch (NullPointerException e){
                        phone=pref.getString("phone", null);
                    }
                    editor.commit();
                    mService= Common.getAPI();
                    mService.checkPurchaseExists(phone,downloadLists.get(position).subject_id)
                            .enqueue(new Callback<CheckPurchaseResponse>() {
                                @Override
                                public void onResponse(Call<CheckPurchaseResponse> call, Response<CheckPurchaseResponse> response) {
                                    CheckPurchaseResponse purchaseResponse = response.body();
                                    if (purchaseResponse.isExists()) {
                                        if (Common.downloadRepository.isExist(Integer.parseInt(downloadLists.get(position).download_id))!=1){
//                                        Toast.makeText(context, "Downloadlist added", Toast.LENGTH_SHORT).show();
                                            classtipDownload downloads=new classtipDownload();
                                            downloads.id= downloadLists.get(position).download_id;
                                            downloads.fileName=downloadLists.get(position).file_name;
                                            downloads.courseId=downloadLists.get(position).course_id;
                                            downloads.subId=downloadLists.get(position).subject_id;
                                            downloads.downloadFlag="N";

                                            Common.downloadRepository.insertDownloads(downloads);
//                                        Toast.makeText(context, new Gson().toJson(Common.downloadRepository.countSubjects()), Toast.LENGTH_SHORT).show();
                                            Log.d("classtip23", new Gson().toJson(downloads));
                                        }
                                        new DownloadTask(context, holder.text_download, downloadLists.get(position).download_url,holder.pDialog);
                                    }
                                    else
                                    {
                                        final AlertDialog.Builder builder=new AlertDialog.Builder(context);
                                        builder.setMessage("You need to purchase for downloading this!!! \n Do You Want To Purchase ?")
                                                .setCancelable(false)
                                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        Intent intent=new Intent(context,PurchaseActivity.class);
                                                        context.startActivity(intent);
                                                    }
                                                }) .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        });
                                        builder.show();
                                    }
                                }
                                @Override
                                public void onFailure(Call<CheckPurchaseResponse> call, Throwable t) {
                                }
                            });
            }
        });
    }

    @Override
    public int getItemCount() {
        return downloadLists.size();
    }

    public void setDownloadList(List<DownloadList> downloadList) {
        this.downloadLists = downloadList;
        notifyDataSetChanged();
    }

    class DownloadViewHolder extends RecyclerView.ViewHolder{
        TextView txt_subject_name,text_download;
        // Progress Dialog
        public ProgressDialog pDialog;

        public DownloadViewHolder(View itemView) {
            super(itemView);
            txt_subject_name=(TextView)itemView.findViewById(R.id.txt_subjct_name);
            text_download=(TextView)itemView.findViewById(R.id.text_download);
        }
    }
}
