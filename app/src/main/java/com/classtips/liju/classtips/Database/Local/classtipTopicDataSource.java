package com.classtips.liju.classtips.Database.Local;

import com.classtips.liju.classtips.Database.DataSource.IclasstipTopicDataSource;
import com.classtips.liju.classtips.Database.ModelDB.classtipTopic;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/8/2018.
 */

public class classtipTopicDataSource implements IclasstipTopicDataSource{

    private classtipTopicDAO topicDAO;
    private static classtipTopicDataSource instance;

    public classtipTopicDataSource(classtipTopicDAO topicDAO) {
        this.topicDAO = topicDAO;
    }

    public static classtipTopicDataSource getInstance(classtipTopicDAO topicDAO){
        if (instance==null)
            instance=new classtipTopicDataSource(topicDAO);
        return instance;
    }

    @Override
    public Flowable<List<classtipTopic>> getTopics() {
        return topicDAO.getTopics();
    }

    @Override
    public Flowable<List<classtipTopic>> getTopicsById(int chapId, String classmodeId) {
        return topicDAO.getTopicsById(chapId, classmodeId);
    }


    @Override
    public Flowable<List<classtipTopic>> getFavoritessById() {
        return topicDAO.getFavoritessById();
    }

    @Override
    public int isExist(int topicId) {
        return topicDAO.isExist(topicId);
    }

    @Override
    public int isViewed(String flag,int topicId) {
        return topicDAO.isViewed(flag,topicId);
    }

    @Override
    public int isUpdated(String flag, int topicId) {
        return topicDAO.isUpdated(flag, topicId);
    }

    @Override
    public int isFavorite(int topicId) {
        return topicDAO.isFavorite(topicId);
    }

    @Override
    public int countTopics() {
        return topicDAO.countTopics();
    }

    @Override
    public void insertTopics(classtipTopic... classtipTopics) {
        topicDAO.insertTopics(classtipTopics);
    }

    @Override
    public void updateTopicsById(String subId, String subName, String chapName, String flag, int topicId) {
        topicDAO.updateTopicsById(subId, subName, chapName, flag, topicId);
    }

    @Override
    public void updateTopics(String flag, int topicId) {
        topicDAO.updateTopics(flag, topicId);
    }

    @Override
    public void updateFavorite(int topicId) {
        topicDAO.updateFavorite(topicId);
    }

    @Override
    public void removeFavorite(int topicId) {
        topicDAO.removeFavorite(topicId);
    }

    @Override
    public void updateclasstipTopics(classtipTopic... classtipTopics) {
        topicDAO.updateclasstipTopics(classtipTopics);
    }
}
