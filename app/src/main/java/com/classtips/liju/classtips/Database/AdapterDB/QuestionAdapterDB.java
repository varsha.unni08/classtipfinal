package com.classtips.liju.classtips.Database.AdapterDB;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.classtips.liju.classtips.Adapter.QuestionViewHolder;
import com.classtips.liju.classtips.Adapter.TopicViewHolder;
import com.classtips.liju.classtips.Database.ModelDB.classtipChapter;
import com.classtips.liju.classtips.Database.ModelDB.classtipQuestion;
import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.Model.CheckPurchaseResponse;
import com.classtips.liju.classtips.QuestionVideoActivity;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.Utils.Common;
import com.classtips.liju.classtips.Utils.DownloadUtils;
import com.classtips.liju.classtips.Utils.ProgressDialog;
import com.classtips.liju.classtips.config.ApplicationConstants;
import com.downloader.Error;
import com.downloader.OnCancelListener;
import com.downloader.OnDownloadListener;
import com.downloader.OnPauseListener;
import com.downloader.OnProgressListener;
import com.downloader.OnStartOrResumeListener;
import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;
import com.downloader.Progress;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by User on 9/27/2018.
 */

public class QuestionAdapterDB extends RecyclerView.Adapter<QuestionViewHolder> {
    Context context;
    List<classtipQuestion> classtipQuestions;

    public QuestionAdapterDB(Context context, List<classtipQuestion> classtipQuestions) {
        this.context = context;
        this.classtipQuestions = classtipQuestions;
    }

    @Override
    public QuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(context).inflate(R.layout.question_item_layout,null);
        return new QuestionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final QuestionViewHolder holder, final int position) {

        String fileName = classtipQuestions.get(position).question_img.substring(classtipQuestions.get(position).question_img.lastIndexOf('/') + 1);

        String fileName1 = classtipQuestions.get(position).answer_img.substring(classtipQuestions.get(position).answer_img.lastIndexOf('/') + 1);

        //String fileName2 = classtipQuestions.get(position).question_name.substring(classtipQuestions.get(position).question_name.lastIndexOf('/') + 1);


        if(!classtipQuestions.get(position).isQuestionimgdownloaded)
        {
            holder.imgdownload.setVisibility(View.VISIBLE);

            Picasso.with(context)
                    .load(classtipQuestions.get(position).question_img)
                    .into(holder.image_product);



        }
        else{

            holder.imgdownload.setVisibility(View.GONE);

            File f1 = new File(context.getExternalCacheDir() + "/" + Common.currentClasstipSubject.subject_id + "/" + Common.currentClasstipTopic.chptr_id + "/" + Common.currentClasstipTopic.topic_id + "/"+classtipQuestions.get(position).question_id+"/"+fileName);


            Picasso.with(context)
                    .load(f1)
                    .into(holder.image_product);
        }

        if(!classtipQuestions.get(position).isAnswerimgdownloaded)
        {
            holder.imgdownload.setVisibility(View.VISIBLE);
        }
        else {
            holder.imgdownload.setVisibility(View.GONE);
        }


        if(!classtipQuestions.get(position).isVideoDownloaded)
        {
            holder.imgdownload.setVisibility(View.VISIBLE);
        }
        else {
            holder.imgdownload.setVisibility(View.GONE);
        }


        holder.text_watch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Common.currentClasstipQuestion=classtipQuestions.get(position);
                Common.currentQuestion=null;

                context.startActivity(new Intent(context,QuestionVideoActivity.class));
            }
        });

        holder.imgdownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(classtipQuestions.get(position).question_type.equalsIgnoreCase("f")) {

                    try {


                        File file_dirq = new File(context.getExternalCacheDir() + "/" + Common.currentClasstipSubject.subject_id + "/" + Common.currentClasstipTopic.chptr_id + "/" + Common.currentClasstipTopic.topic_id + "/" + classtipQuestions.get(position).question_id);


                        if (file_dirq.exists()) {
                            file_dirq.delete();
                        } else {
                            file_dirq.mkdirs();

                        }


                        String url_qvideo = DownloadUtils.PHYSICS_TOPICURL + Common.currentClasstipTopic.chptr_id + "/" + Common.currentClasstipTopic.topic_id + "/" + ApplicationConstants.qvideo + "/" + classtipQuestions.get(position).question_name;



                        downloadFile(url_qvideo, file_dirq.getAbsolutePath(), classtipQuestions.get(position).question_name, position);


                        String fileName = classtipQuestions.get(position).question_img.substring(classtipQuestions.get(position).question_img.lastIndexOf('/') + 1);

                        // String timeStamp = "QUESTION"+String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));

                        File ftodownload = new File(file_dirq, fileName);


                        new DownloadImage(position,classtipQuestions.get(position).question_img, ftodownload.getAbsolutePath()).execute("");


                        //String timeStam = "ANSWER"+String.valueOf(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()));


                        String fileNameimg = classtipQuestions.get(position).answer_img.substring(classtipQuestions.get(position).answer_img.lastIndexOf('/') + 1);


                        File ftodownloadanswer = new File(file_dirq, fileNameimg);


                        new DownloadImage(position,classtipQuestions.get(position).answer_img, ftodownloadanswer.getAbsolutePath()).execute("");

                    }catch (Exception e)
                    {

                    }
                }
                else {

                    Toast.makeText(context,"please purchase the subject",Toast.LENGTH_SHORT).show();
                }

            }
        });


        holder.text_answer_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(context, "clicked", Toast.LENGTH_SHORT).show();

                AlertDialog.Builder answerView = new AlertDialog.Builder(context);
                LayoutInflater factory = LayoutInflater.from(v.getRootView().getContext());
                final View view = factory.inflate(R.layout.question_answer_layout, null);
                ImageView img_answer;
                img_answer=view.findViewById(R.id.img_answer);

                try{

                    String fileName1 = classtipQuestions.get(position).answer_img.substring(classtipQuestions.get(position).answer_img.lastIndexOf('/') + 1);


                    if(!iscontentsexist(position,fileName1))
                    {
                        holder.imgdownload.setVisibility(View.VISIBLE);

                        Picasso.with(context)
                                .load(classtipQuestions.get(position).answer_img)
                                .into(img_answer);

                    }
                    else{

                        File f1 = new File(context.getExternalCacheDir() + "/" + Common.currentClasstipSubject.subject_id + "/" + Common.currentClasstipTopic.chptr_id + "/" + Common.currentClasstipTopic.topic_id + "/"+classtipQuestions.get(position).question_id+"/"+fileName1);


                        Picasso.with(context)
                                .load(f1)
                                .into(img_answer);
                    }


//                    Picasso.with(context)
//                            .load(classtipQuestions.get(position).answer_img)
//                            .into(img_answer);
                }catch (Exception e){

                }

                answerView.setView(view);
                answerView.show();
//
            }
        });

        holder.image_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder zoomView = new AlertDialog.Builder(context);
                LayoutInflater factory = LayoutInflater.from(v.getRootView().getContext());
                final View view = factory.inflate(R.layout.zooming_layout, null);
//                ImageView image_product;
                PhotoView photoView;
                photoView=view.findViewById(R.id.photo_view);


                String fileName = classtipQuestions.get(position).question_img.substring(classtipQuestions.get(position).question_img.lastIndexOf('/') + 1);

               // String fileName1 = classtipQuestions.get(position).answer_img.substring(classtipQuestions.get(position).answer_img.lastIndexOf('/') + 1);

                //String fileName2 = classtipQuestions.get(position).question_name.substring(classtipQuestions.get(position).question_name.lastIndexOf('/') + 1);


                if(!iscontentsexist(position,fileName))
                {
                    holder.imgdownload.setVisibility(View.VISIBLE);

                    Picasso.with(context)
                            .load(classtipQuestions.get(position).question_img)
                            .into(photoView);

                }
                else{

                    File f1 = new File(context.getExternalCacheDir() + "/" + Common.currentClasstipSubject.subject_id + "/" + Common.currentClasstipTopic.chptr_id + "/" + Common.currentClasstipTopic.topic_id + "/"+classtipQuestions.get(position).question_id+"/"+fileName);


                    Picasso.with(context)
                            .load(f1)
                            .into(photoView);
                }


                zoomView.setView(view);

                zoomView.show();
//                Intent intent=new Intent(context,ZoomingActivity.class);
//                intent.putExtra("name",questions.get(position).question_img);
//                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return classtipQuestions.size();
    }

    public void setQuestionList(List<classtipQuestion> classtipQuestions) {
        this.classtipQuestions = classtipQuestions;
        notifyDataSetChanged();
    }


    public boolean iscontentsexist(int position,String filename)
    {

        boolean a=false;
        File file_dirq = new File(context.getExternalCacheDir() + "/" + Common.currentClasstipSubject.subject_id + "/" + Common.currentClasstipTopic.chptr_id + "/" + Common.currentClasstipTopic.topic_id + "/"+classtipQuestions.get(position).question_id);

        if(file_dirq.exists())
        {
            if(DownloadUtils.getFilelist(file_dirq).size()>0)
            {

                for (File file :DownloadUtils.getFilelist(file_dirq))
                {

                    if(file.getName().contains(filename))
                    {
                        a=true;
                        break;

                    }


                }


            }

        }



        return  a;
    }


    class DownloadImage extends AsyncTask<String,String,File> {

        String url="";
        String path="";
        int position;

        ProgressDialog progressDialog=new ProgressDialog(context);


        public DownloadImage(int position,String url, String path) {
            this.url = url;
            this.path = path;
            this.position=position;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.show();

        }
        @Override
        protected File doInBackground(String... URL) {
            String imageURL = url;
            Bitmap bitmap = null;

            File f=null;
            try {
                // Download Image from URL
                InputStream input = new java.net.URL(imageURL).openStream();
                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);

                int w = (int) context.getResources().getDimension(R.dimen.dimen_150dp);

                Bitmap bitmap_converted = bitmap;


                f = new File(path);

                if(f.exists())
                {
                    f.delete();
                }
                f.createNewFile();

//Convert bitmap to byte array
                //  Bitmap bitmap = bitmap_converted;
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap_converted.compress(Bitmap.CompressFormat.PNG, 100 /*ignored for PNG*/, bos);
                byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
                FileOutputStream fos = new FileOutputStream(f);
                fos.write(bitmapdata);
                fos.flush();
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return f;
        }
        @Override
        protected void onPostExecute(File result) {

            progressDialog.dismiss();

            String fileName = classtipQuestions.get(position).answer_img.substring(classtipQuestions.get(position).answer_img.lastIndexOf('/') + 1);

            String fileName1 = classtipQuestions.get(position).question_img.substring(classtipQuestions.get(position).question_img.lastIndexOf('/') + 1);

            if(result.getName().equalsIgnoreCase(fileName))
            {

                classtipQuestions.get(position).isAnswerimgdownloaded=true;
            }

            if(result.getName().equalsIgnoreCase(fileName1))
            {

                classtipQuestions.get(position).isQuestionimgdownloaded=true;
            }

notifyDataSetChanged();

        }
    }


    public void downloadFile(final String url, final String filepath, final String file,  final int pos) {


        final ProgressDialog progressDialog=new ProgressDialog(context);


        PRDownloaderConfig config1 = PRDownloaderConfig.newBuilder()
                .setReadTimeout(30_000)
                .setConnectTimeout(30_000)
                .build();
        PRDownloader.initialize(context, config1);

        int downloadId = PRDownloader.download(url, filepath, file)
                .build().setOnStartOrResumeListener(new OnStartOrResumeListener() {
                    @Override
                    public void onStartOrResume() {
                        progressDialog.show();

                    }
                }).setOnPauseListener(new OnPauseListener() {
                    @Override
                    public void onPause() {

                        progressDialog.dismiss();
                        Toast.makeText(context,"download paused",Toast.LENGTH_SHORT).show();

                    }
                }).setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel() {
                        progressDialog.dismiss();
                        Toast.makeText(context,"download cancelled",Toast.LENGTH_SHORT).show();

                    }
                }).setOnProgressListener(new OnProgressListener() {
                    @Override
                    public void onProgress(Progress progress) {

                    }
                }).start(new OnDownloadListener() {
                    @Override
                    public void onDownloadComplete() {
                        progressDialog.dismiss();
                        Toast.makeText(context,"download completed",Toast.LENGTH_SHORT).show();

                        File f_download = new File(filepath, file);


                        //DownloadUtils.splitvideo(f_download);

                        classtipQuestions.get(pos).isVideoDownloaded=true;

                        new SplitFileAsync(f_download).execute("");

                    }

                    @Override
                    public void onError(Error error) {
                        progressDialog.dismiss();

                        if(DownloadUtils.isConnectingToInternet(context))
                        {

                            Toast.makeText(context,"Cannot download video",Toast.LENGTH_SHORT).show();
                        }
                        else{

                            Toast.makeText(context,"please check internet connection",Toast.LENGTH_SHORT).show();
                        }


                        notifyDataSetChanged();
                    }
                });

    }



    public class  SplitFileAsync extends AsyncTask<String,String,String>{


        File f;
        ProgressDialog progressDialog=new ProgressDialog(context);


        public SplitFileAsync(File f) {
            this.f = f;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {

                int partCounter = 1;//I like to name parts from 001, 002, 003, ...
                //you can change it to 0 if you want 000, 001, ...

                int sizeOfFiles = 1024*100 ;// 100kb
                byte[] buffer = new byte[sizeOfFiles];

                String fileName = f.getName();

                FileInputStream fis = new FileInputStream(f);
                BufferedInputStream bis = new BufferedInputStream(fis);

                int bytesAmount = 0;
                while ((bytesAmount = bis.read(buffer)) > 0) {



                    String filePartName = String.format("%s.%03d", fileName, partCounter++);
                    File newFile = new File(f.getParent(), filePartName);

                    FileOutputStream out = new FileOutputStream(newFile);
                    out.write(buffer, 0, bytesAmount);
                }

            }catch (Exception e)
            {

            }
            return null;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            if(f.exists())
            {
                f.delete();
            }
           // notifyItemRangeChanged(0,classtipQuestions.size());

            notifyDataSetChanged();
        }
    }


}
