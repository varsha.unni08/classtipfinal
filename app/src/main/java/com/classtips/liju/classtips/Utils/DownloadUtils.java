package com.classtips.liju.classtips.Utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.classtips.liju.classtips.ChapterActivity;
import com.classtips.liju.classtips.R;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class DownloadUtils {

    public static final String CIPHER_ALGORITHM = "AES/CBC/PKCS5Padding";
    public static final String KEY_SPEC_ALGORITHM = "AES";
    public static final String PROVIDER = "BC";

    public static final String SECRET_KEY = "SECRET_KEY";

    public static final String PHYSICS_TOPICURL="https://classtips.in/classtip/PHYSICS/";


    public static SecretKey generateKey() throws Exception
    {
        String key= "JaNdRgUkXp2s5v8y/B?E(G+KbPeShVmY";

        if (null == key || key.isEmpty()) {
            SecureRandom secureRandom = new SecureRandom();
            KeyGenerator keyGenerator = null;
            try {
                keyGenerator = KeyGenerator.getInstance(KEY_SPEC_ALGORITHM);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            keyGenerator.init(256, secureRandom);
            SecretKey secretKey = keyGenerator.generateKey();
            // saveSecretKey(secretKey);
            return secretKey;
        }


        byte[] decodedKey = key.getBytes();
        SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, KEY_SPEC_ALGORITHM);
        return originalKey;
    }


    public static byte[] encode(SecretKey yourKey, byte[] fileData)
            throws Exception {
        byte[] data = yourKey.getEncoded();
        SecretKeySpec skeySpec = new SecretKeySpec(data, 0, data.length, KEY_SPEC_ALGORITHM);
        Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM, PROVIDER);
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, new IvParameterSpec(new byte[cipher.getBlockSize()]));
        return cipher.doFinal(fileData);

    }

    public static byte[] decode(SecretKey yourKey, byte[] fileData)
            throws Exception {
        byte[] decrypted;
        Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM, PROVIDER);
        cipher.init(Cipher.DECRYPT_MODE, yourKey, new IvParameterSpec(new byte[cipher.getBlockSize()]));
        decrypted = cipher.doFinal(fileData);
        return decrypted;
    }

    public static void saveFile(byte[] encodedBytes, String path) {
        try {
            File file = new File(path);
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
            bos.write(encodedBytes);
            bos.flush();
            bos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static void splitvideo(File f)
    {

        try {

            int partCounter = 1;//I like to name parts from 001, 002, 003, ...
            //you can change it to 0 if you want 000, 001, ...

            int sizeOfFiles = 1024*100 ;// 100kb
            byte[] buffer = new byte[sizeOfFiles];

            String fileName = f.getName();

            FileInputStream fis = new FileInputStream(f);
            BufferedInputStream bis = new BufferedInputStream(fis);

            int bytesAmount = 0;
            while ((bytesAmount = bis.read(buffer)) > 0) {



                String filePartName = String.format("%s.%03d", fileName, partCounter++);
                File newFile = new File(f.getParent(), filePartName);

                FileOutputStream out = new FileOutputStream(newFile);
                out.write(buffer, 0, bytesAmount);
            }

        }catch (Exception e)
        {

        }

    }




    public static void mergeFiles(List<File> files, File into)
            throws IOException {
        try {

            if(!into.exists())
            {

                into.createNewFile();
            }

            BufferedInputStream in=null;

            FileOutputStream fos = new FileOutputStream(into);
            BufferedOutputStream mergingStream = new BufferedOutputStream(fos) ;
            for(File f :files)

            {


                FileInputStream fileInputStream=new FileInputStream(f);
                in=new BufferedInputStream(fileInputStream);
                byte[] buf = new byte[1024*1024];
                int len;

                while ((len = in.read(buf)) > 0) {
                    mergingStream.write(buf, 0, len);
                }



                //mergingStream.write();
            }

            in.close();
            mergingStream.close();
        }
        catch (Exception e)
        {
            Log.e("Tagggggg",e.toString());
        }





    }


    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager)context. getSystemService(ChapterActivity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }


    public static void showCheckConnectiondialog(Context context)
    {


        AlertDialog.Builder builder=new AlertDialog.Builder(context);
        builder.setTitle(R.string.app_name);
        builder.setMessage("Please check internet connection");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        builder.show();

    }


    public static List<File>  getFilelist(File directoryPath)
    {

        List<File>files=new ArrayList<>();

        try {



            //File file = new File(getExternalCacheDir().getPath());
            // File directoryPath = new File();
            //List of all files and directories
            String contents[] = directoryPath.list();

            Arrays.sort(contents);
            // System.out.println("List of files and directories in the specified directory:");
            for (int i = 0; i < contents.length; i++) {

                File file=new File(directoryPath+"/"+contents[i]);

                // String fileName = file.getName().replace(".bat",".mp4");

                files.add(file);


                // Log.e("Splited videos",file.getName());

                // System.out.println(contents[i]);
            }

        }catch (Exception e)
        {

        }

        return files;
    }





    public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
                matrix, false);

        return resizedBitmap;
    }




}
