package com.classtips.liju.classtips;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.classtips.liju.classtips.Model.QRcode;
import com.classtips.liju.classtips.Model.Wallet;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QRcodeActivity extends AppCompatActivity {

    SurfaceView cameraQRcode;
    TextView textQR;
    EditText serial_no;
    Button btn_qrcode;
    BarcodeDetector barcodeDetector;
    CameraSource cameraSource;
    final  int RequestCameraPermissionID = 1001;
    String key,price,phone;

    Wallet wallet;
    IClassTipAPI mService;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case RequestCameraPermissionID: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                        return;
                    }
                    try {
                        cameraSource.start(cameraQRcode.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);
        if (isConnectingToInternet()){

        mService= Common.getAPI();
        cameraQRcode = (SurfaceView) findViewById(R.id.camera_qrcode);
        textQR = (TextView) findViewById(R.id.txt_qrcode);

        key=getIntent().getStringExtra("key");
        price=getIntent().getStringExtra("price");
        if (key==null)
        {
            key="0";
        }
       // Toast.makeText(this, key, Toast.LENGTH_SHORT).show();

        serial_no=(EditText)findViewById(R.id.edt_text_serial);
        btn_qrcode=(Button)findViewById(R.id.btn_qrcode) ;

            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            //Toast.makeText(this, "testing..."+pref.getString("phone", null), Toast.LENGTH_SHORT).show();
            try {
                phone = Common.currentUser.getPhone();
            } catch (NullPointerException e) {
                phone = pref.getString("phone", null);
            }
            editor.commit();

        barcodeDetector = new BarcodeDetector.Builder(this)
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();
        cameraSource = new CameraSource
                .Builder(this, barcodeDetector)
                .setRequestedPreviewSize(640, 480)
                .build();

        cameraQRcode.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                if (ActivityCompat.checkSelfPermission(QRcodeActivity.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    //Request permission
                    ActivityCompat.requestPermissions(QRcodeActivity.this,
                            new String[]{Manifest.permission.CAMERA},RequestCameraPermissionID);
                    return;
                }
                try {
                    cameraSource.start(cameraQRcode.getHolder());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });

        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void release() {

            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {
                final SparseArray<Barcode> qrcodes=detections.getDetectedItems();
                if (qrcodes.size()!=0)
                {
                    textQR.post(new Runnable() {
                        @Override
                        public void run() {
                            Vibrator vibrator=(Vibrator)getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                            vibrator.vibrate(1000);
                            //textQR.setText(qrcodes.valueAt(0).displayValue);
                            textQR.setText("QR Code Scanning Completed");

                            btn_qrcode.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                   // Toast.makeText(QRcodeActivity.this, serial_no.getText(), Toast.LENGTH_SHORT).show();
//                                    serial_no.getText();
                                    mService.getQRcode(String.valueOf(qrcodes.valueAt(0).displayValue), String.valueOf(serial_no.getText()),phone)
                                            .enqueue(new Callback<QRcode>() {
                                                @Override
                                                public void onResponse(Call<QRcode> call, Response<QRcode> response) {
                                                    //Toast.makeText(QRcodeActivity.this, "added", Toast.LENGTH_SHORT).show();
                                                    Common.currentQRcode=response.body();
                                                    QRcode qRcode=response.body();

                                                    //Toast.makeText(QRcodeActivity.this, Common.currentQRcode.getQrprice(), Toast.LENGTH_SHORT).show();
                                                    if(qRcode.isUpdated())
                                                    {
                                                        Toast.makeText(QRcodeActivity.this, "Qr code readed successfully", Toast.LENGTH_SHORT).show();

                                                        if (key.equals("1"))
                                                        {
                                                            Intent intent=new Intent(QRcodeActivity.this,PurchaseActivity.class);
                                                            startActivity(intent);
                                                            finish();

                                                        }
                                                        else if (key.equals("2"))
                                                        {

                                                            mService.getWalletInformation(phone)
                                                                    .enqueue(new Callback<Wallet>() {
                                                                        @Override
                                                                        public void onResponse(Call<Wallet> call, Response<Wallet> response) {
                                                                            wallet=response.body();

                                                                            if (Integer.parseInt(price)<=Integer.parseInt(wallet.getWalletmoney())){
                                                                                Intent intent=new Intent(QRcodeActivity.this,CartActivity.class);
                                                                                intent.putExtra("key","2");
                                                                                intent.putExtra("walletMoney",wallet.getWalletmoney());
                                                                                startActivity(intent);
                                                                                finish();
                                                                            }
                                                                            else
                                                                            {
                                                                                Toast.makeText(QRcodeActivity.this, "Low wallet money for purchase", Toast.LENGTH_SHORT).show();
                                                                                showUpdateDialogue();
                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void onFailure(Call<Wallet> call, Throwable t) {

                                                                        }
                                                                    });


                                                        }
                                                        else if (key.equals("3"))
                                                        {
                                                            Intent intent=new Intent(QRcodeActivity.this,WalletActivity.class);
                                                            startActivity(intent);
                                                            finish();
                                                        }
                                                        else
                                                        {
                                                            finish();
                                                        }


                                                    }
                                                    else
                                                    {
                                                        Toast.makeText(QRcodeActivity.this, "already used", Toast.LENGTH_SHORT).show();
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<QRcode> call, Throwable t) {

                                                }
                                            });
                                }
                            });

                        }
                    });
                }
            }
        });
    }
    else
            Toast.makeText(this, "Sorry you are offline", Toast.LENGTH_SHORT).show();

    }

    private void showUpdateDialogue() {
        final AlertDialog.Builder builder=new AlertDialog.Builder(QRcodeActivity.this);
        builder.setTitle("Low Wallet Money");
        View itemView= LayoutInflater.from(QRcodeActivity.this)
                .inflate(R.layout.purchase_layout,null);

        builder.setView(itemView);

        builder.setMessage("Do You Want To READ another QR CODE to update Your Wallet ?")
                .setCancelable(false)
                .setPositiveButton("Update Wallet", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                }) .setNegativeButton("Later", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent=new Intent(QRcodeActivity.this,CartActivity.class);
                startActivity(intent);
                finish();

            }
        });
        builder.show();
    }

    private boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(ChapterActivity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
