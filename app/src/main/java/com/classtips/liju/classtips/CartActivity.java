package com.classtips.liju.classtips;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.classtips.liju.classtips.Adapter.CartAdapter;
import com.classtips.liju.classtips.Database.ModelDB.classtipPurchase;
import com.classtips.liju.classtips.Database.ModelDB.classtipUser;
import com.classtips.liju.classtips.Model.UpdateWallet;
import com.classtips.liju.classtips.Model.Wallet;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;
import com.classtips.liju.classtips.Utils.RecyclerItemTouchHelper;
import com.classtips.liju.classtips.Utils.RecyclerItemTouchHelperListener;
import com.google.gson.Gson;
import com.yarolegovich.lovelydialog.LovelyChoiceDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CartActivity extends AppCompatActivity implements RecyclerItemTouchHelperListener {
    RecyclerView recyclerCart;
    Button btn_place_order,btn_add_subject;
    CompositeDisposable compositeDisposable;
    Wallet wallet;
    int money;
    String phone,key,walletMoney,totalPrice;


    RelativeLayout rootLayout;
    CartAdapter cartAdapter;
    IClassTipAPI mService;
    List<classtipPurchase> localPurchases=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        compositeDisposable=new CompositeDisposable();
        mService= Common.getAPI();
        recyclerCart=(RecyclerView)findViewById(R.id.recycler_cart);
        recyclerCart.setLayoutManager(new LinearLayoutManager(this));
        recyclerCart.setHasFixedSize(true);

        rootLayout=(RelativeLayout) findViewById(R.id.rootLayout);
        btn_place_order=(Button)findViewById(R.id.btn_place_order);
        btn_add_subject=(Button)findViewById(R.id.btn_add_subject);

        key=getIntent().getStringExtra("key");
        walletMoney=getIntent().getStringExtra("walletMoney");
        if (isConnectingToInternet()) {
            if (key == null) {
                key = "0";
            }

            if (key.equals("2")) {
                placeOrder();
            }
        }
        compositeDisposable.add(Common.userRepository.getUsers()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<classtipUser>>() {
                    @Override
                    public void accept(List<classtipUser> classtipUsers) throws Exception {
                        Log.d("classtip234", new Gson().toJson(classtipUsers));
                        try {
                            JSONArray req = new JSONArray(new Gson().toJson(classtipUsers));
                            for (int i = 0; i < req.length(); ++i) {
                                JSONObject rec = req.getJSONObject(i);
                                phone = rec.getString("phone");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }));
        compositeDisposable.add(Common.purchaseRepository.getPurchaseList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<classtipPurchase>>() {
                    @Override
                    public void accept(List<classtipPurchase> classtipPurchases) throws Exception {
                        if (classtipPurchases.size()==0){
                            btn_place_order.setEnabled(false);
                            showPurchaseDialog();
                        }
                        else{ if (isConnectingToInternet()){
                            btn_place_order.setEnabled(true);
                        }else {
                            btn_place_order.setEnabled(false);
                            btn_add_subject.setEnabled(false);
                            Toast.makeText(CartActivity.this, "Oops!! You have no internet connection   ", Toast.LENGTH_SHORT).show();
                        }

                        }

                    }
                }));

        btn_add_subject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(CartActivity.this,PurchaseActivity.class);
                startActivity(intent);
                finish();

            }
        });

        btn_place_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new LovelyChoiceDialog(CartActivity.this)
                        .setTitle("Payment Options")
                        .setMessage("Choose a payment method")
                        .setItems(generateString(), new LovelyChoiceDialog.OnItemSelectedListener<String>() {
                            @Override
                            public void onItemSelected(int position, String item) {
                                if (item=="Wallet"){
                                    mService.getWalletInformation(phone)
                                            .enqueue(new Callback<Wallet>() {
                                                @Override
                                                public void onResponse(Call<Wallet> call, Response<Wallet> response) {
                                                    wallet=response.body();
                                                    if (Common.purchaseRepository.sumPrice()<=Integer.parseInt(wallet.getWalletmoney())){
//                                                        Toast.makeText(CartActivity.this, "true", Toast.LENGTH_SHORT).show();
                                                        placeOrder();
                                                    }
                                                    else
                                                    {
                                                        Toast.makeText(CartActivity.this, "Your Wallet Money Is Low.. Kindly Update Your Wallet", Toast.LENGTH_SHORT).show();
                                                        showUpdateDialogue();
                                                    }



                                                }

                                                @Override
                                                public void onFailure(Call<Wallet> call, Throwable t) {

                                                }
                                            });
                                }
                            }
                        }).show();




            }
        });

        ItemTouchHelper.SimpleCallback simpleCallback=new RecyclerItemTouchHelper(0,ItemTouchHelper.LEFT,this);
        new ItemTouchHelper(simpleCallback).attachToRecyclerView(recyclerCart);

        loadCartItems();
    }

    private void showPurchaseDialog() {

        final AlertDialog.Builder builder=new AlertDialog.Builder(CartActivity.this);
        builder.setMessage("Your Cart Is Empty. \n Do You Want To Purchase Any Subjects ?")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent=new Intent(CartActivity.this,PurchaseActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }) .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();


    }

    @Override
    protected void onResume() {
        super.onResume();
        loadCartItems();
    }

    private List<String> generateString() {
        List<String> result=new ArrayList<>();
        result.add("Net Banking");
        result.add("Debit/Credit Cards");
        result.add("Wallet");
        return result;
    }

    private void showUpdateDialogue() {
        final AlertDialog.Builder builder=new AlertDialog.Builder(CartActivity.this);
        builder.setTitle("Wallet Updation");
        View itemView= LayoutInflater.from(CartActivity.this)
                .inflate(R.layout.purchase_layout,null);

        builder.setView(itemView);

        builder.setMessage("Do You Want To UPDATE Your Wallet ?")
                .setCancelable(false)
                .setPositiveButton("Update Wallet", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent=new Intent(CartActivity.this,QRcodeActivity.class);
                        intent.putExtra("key","2");
                        intent.putExtra("price",String.valueOf(Common.purchaseRepository.sumPrice()));
                        startActivity(intent);
                        finish();
                    }
                }) .setNegativeButton("Later", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void placeOrder() {

        compositeDisposable.add(Common.purchaseRepository.getPurchaseList()
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(new Consumer<List<classtipPurchase>>() {
            @Override
            public void accept(List<classtipPurchase> classtipPurchases) throws Exception {
                sendToServer(classtipPurchases);
            }
        }));
    }

    private void sendToServer(List<classtipPurchase> classtipPurchases) {

        if (classtipPurchases.size()>0)
        {
            final String orderDetail=new Gson().toJson(classtipPurchases);
            Log.d("classtip23", orderDetail);
            //Toast.makeText(this, orderDetail, Toast.LENGTH_SHORT).show();
            //Toast.makeText(this, new Gson().toJson(Common.purchaseRepository.sumPrice()), Toast.LENGTH_SHORT).show();
            mService.newOrder(orderDetail)
                    .enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, Response<String> response) {

                            if (key.equals("2"))
                            {
                                money=Integer.parseInt(walletMoney)-Common.purchaseRepository.sumPrice();

                            }else
                            {
                                money=Integer.parseInt(wallet.getWalletmoney())-Common.purchaseRepository.sumPrice();
                            }
                            Common.purchaseRepository.emptyPurchases();

//                            Toast.makeText(CartActivity.this, "money"+money, Toast.LENGTH_SHORT).show();
                            mService.updateWalet(phone, String.valueOf(money))
                                    .enqueue(new Callback<UpdateWallet>() {
                                        @Override
                                        public void onResponse(Call<UpdateWallet> call, Response<UpdateWallet> response) {

                                           final AlertDialog.Builder builder;
                                           builder=new AlertDialog.Builder(CartActivity.this);
                                           builder.setMessage("Your Purchase is Successfully completed");
                                           builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                               @Override
                                               public void onClick(DialogInterface dialog, int which) {
                                                   dialog.dismiss();
                                                   finish();
                                               }
                                           });
                                            builder.show();
                                        }

                                        @Override
                                        public void onFailure(Call<UpdateWallet> call, Throwable t) {

                                        }
                                    });
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            Toast.makeText(CartActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
        }
    }

    private void loadCartItems() {
        compositeDisposable.add(Common.purchaseRepository.getPurchaseList()
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(new Consumer<List<classtipPurchase>>() {
            @Override
            public void accept(List<classtipPurchase> classtipPurchases) throws Exception {
                displayCartItem(classtipPurchases);
            }
        }));
    }

    private void displayCartItem(List<classtipPurchase> classtipPurchases) {
        localPurchases=classtipPurchases;
        cartAdapter=new CartAdapter(this,classtipPurchases);
        recyclerCart.setAdapter(cartAdapter);
    }

    @Override
    protected void onDestroy() {
        compositeDisposable.clear();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        compositeDisposable.clear();
        super.onStop();
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof CartAdapter.CartViewHolder)
        {
            String name=localPurchases.get(viewHolder.getAdapterPosition()).subjectName;
            final classtipPurchase deletedItem=localPurchases.get(viewHolder.getAdapterPosition());

            final int deletedIndex=viewHolder.getAdapterPosition();

            //Delete item from the adapter
            cartAdapter.removeItem(deletedIndex);
            //Delete item fromRoom Database
            Common.purchaseRepository.deletePurchases(deletedItem);

            Snackbar snackbar=Snackbar.make(rootLayout,new StringBuilder(name).append(" removed from Cart").toString(),Snackbar.LENGTH_LONG);
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cartAdapter.restoreItem(deletedItem,deletedIndex);
                    Common.purchaseRepository.insertPurchases(deletedItem);
                }
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(ChapterActivity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }
}
