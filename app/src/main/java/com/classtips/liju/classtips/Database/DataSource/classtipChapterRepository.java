package com.classtips.liju.classtips.Database.DataSource;

import com.classtips.liju.classtips.Database.ModelDB.classtipChapter;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/7/2018.
 */

public class classtipChapterRepository implements IclasstipChapterDataSource {
    private IclasstipChapterDataSource iclasstipChapterDataSource;
    private static classtipChapterRepository instance;

    public classtipChapterRepository(IclasstipChapterDataSource iclasstipChapterDataSource) {
        this.iclasstipChapterDataSource = iclasstipChapterDataSource;
    }

    public static classtipChapterRepository getInstance(IclasstipChapterDataSource iclasstipChapterDataSource) {
        if (instance==null)
            instance=new classtipChapterRepository(iclasstipChapterDataSource);
        return instance;
    }

    @Override
    public Flowable<List<classtipChapter>> getChapters() {
        return iclasstipChapterDataSource.getChapters();
    }

    @Override
    public Flowable<List<classtipChapter>> getChaptersById(int subId) {
        return iclasstipChapterDataSource.getChaptersById(subId);
    }

    @Override
    public int isExist(int chapId) {
        return iclasstipChapterDataSource.isExist(chapId);
    }

    @Override
    public int countChapters() {
        return iclasstipChapterDataSource.countChapters();
    }

    @Override
    public void emptyChapters() {
        iclasstipChapterDataSource.emptyChapters();
    }

    @Override
    public void updateChapters(classtipChapter... classtipChapters) {
        iclasstipChapterDataSource.updateChapters(classtipChapters);
    }

    @Override
    public void insertChapters(classtipChapter... classtipChapters) {
        iclasstipChapterDataSource.insertChapters(classtipChapters);
    }
}
