package com.classtips.liju.classtips;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.classtips.liju.classtips.Adapter.ExamAdapter;
import com.classtips.liju.classtips.Adapter.QuestionYearAdapter;
import com.classtips.liju.classtips.Database.AdapterDB.ExamAdapterDB;
import com.classtips.liju.classtips.Database.AdapterDB.QuestionYearAdapterDB;
import com.classtips.liju.classtips.Database.ModelDB.classtipExam;
import com.classtips.liju.classtips.Database.ModelDB.classtipQuestionYear;
import com.classtips.liju.classtips.Model.Exams;
import com.classtips.liju.classtips.Model.QuestionYear;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class QuestionYearActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    IClassTipAPI mService;
    RecyclerView question_menu;
    private Spinner spinner_exam;
    String courseId;
    String subjectId;


    //RxJava
    CompositeDisposable compositeDisposable=new CompositeDisposable();

    List<Exams> exams;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_year);


        mService= Common.getAPI();
        question_menu=(RecyclerView)findViewById(R.id.question_menu);
        question_menu.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        question_menu.setHasFixedSize(true);
        spinner_exam=(Spinner)findViewById(R.id.spinner_exam) ;
        spinner_exam.setOnItemSelectedListener(this);

        try {
            courseId=Common.currentUser.getCourse();
        }catch (NullPointerException e){

            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            courseId=pref.getString("courseId", null);
            editor.commit();
        }

        try {
            subjectId=Common.currentSubject.subject_id;
        }catch (NullPointerException e){

            subjectId=Common.currentClasstipSubject.subject_id;
        }


        if (isConnectingToInternet())
        {

            compositeDisposable.add(mService.getExams(subjectId,courseId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<List<Exams>>() {
                        @Override
                        public void accept(List<Exams> exams) throws Exception {
                            ExamAdapter adapter = new ExamAdapter(QuestionYearActivity.this,
                                    R.layout.exam_list_layout, exams);
                            spinner_exam.setAdapter(adapter);

                            try {Log.d("classtip234", new Gson().toJson(exams));
                                JSONArray req = new JSONArray(new Gson().toJson(exams));
                                for (int i = 0; i < req.length(); ++i) {
                                    JSONObject rec = req.getJSONObject(i);
                                    String id = String.valueOf(rec.getInt("exam_id"));
                                    String courseId= String.valueOf(rec.getInt("course_id"));
                                    String subId= String.valueOf(rec.getInt("sub_id"));
                                    String examName = rec.getString("exam_name");
                                    //Toast.makeText(QuestionYearActivity.this, "testing.."+examName, Toast.LENGTH_SHORT).show();

                                    //String id = String.valueOf(rec.getInt("exam_id"));


                                    if (Common.examRepository.isExist(Integer.parseInt(id))!=1)
                                    {

                                        classtipExam exam=new classtipExam();
                                        exam.id= id;
                                        exam.examName=examName;
                                        exam.courseId=courseId;
                                        exam.subId=subId;

                                        Common.examRepository.insertExams(exam);
                                        Toast.makeText(QuestionYearActivity.this, "exam"+new Gson().toJson(Common.examRepository.countExams()), Toast.LENGTH_SHORT).show();
                                        Log.d("classtip2345", new Gson().toJson(exam));
                                        Log.d("classtip2345", new Gson().toJson(Common.examRepository.countExams()));
                                    }
                                    else
                                    {

                                    }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    }));
        }
        else
        {
            compositeDisposable.add(Common.examRepository.getExamsById(Integer.parseInt(courseId), Integer.parseInt(subjectId))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Consumer<List<classtipExam>>() {
                        @Override
                        public void accept(List<classtipExam> classtipExams) throws Exception {
                            ExamAdapterDB adapter = new ExamAdapterDB(QuestionYearActivity.this,
                                    R.layout.exam_list_layout, classtipExams);
                            spinner_exam.setAdapter(adapter);
                        }
                    }));
        }

    }

    private void loadQuestionListDB(String examId) {
        compositeDisposable.add(Common.questionYearRepository.getQuestionsById(Integer.parseInt(examId))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<classtipQuestionYear>>() {
                    @Override
                    public void accept(List<classtipQuestionYear> classtipQuestionYears) throws Exception {
                        displayQuestionListDB(classtipQuestionYears);
                    }
                }));
    }

    private void displayQuestionListDB(List<classtipQuestionYear> classtipQuestionYears) {
        QuestionYearAdapterDB adapter=new QuestionYearAdapterDB(this,classtipQuestionYears);
        question_menu.setAdapter(adapter);
    }

    private void loadQuestionList(String examId) {

        compositeDisposable.add(mService.getQuestionYear(examId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<QuestionYear>>() {
                    @Override
                    public void accept(List<QuestionYear> questionYears) throws Exception {
                        displayQuestionList(questionYears);
                        try {
                            JSONArray req = new JSONArray(new Gson().toJson(questionYears));
                            Log.d("classtip23", new Gson().toJson(questionYears));
                            for (int i = 0; i < req.length(); ++i) {
                                JSONObject rec = req.getJSONObject(i);
                                int id= rec.getInt("question_id");
                                int examId= rec.getInt("exam_id");
                                int groupNo= rec.getInt("group_no");
                                int topicId= rec.getInt("topic_id");
                                String question = rec.getString("question_img");
                                String questionType = rec.getString("question_type");
                                String questionVideo=rec.getString("question_video");
                                String orderNo=rec.getString("order_no");
                                String answerImage=rec.getString("answer_img");
                                if (Common.questionYearRepository.isExist(id)!=1)
                                {
                                    //Toast.makeText(QuestionYearActivity.this, "classmode added", Toast.LENGTH_SHORT).show();
                                    classtipQuestionYear classtipQuestion =new classtipQuestionYear();
                                    classtipQuestion.id= String.valueOf(id);
                                    classtipQuestion.questionVideo=questionVideo;
                                    classtipQuestion.examId= String.valueOf(examId);
                                    classtipQuestion.topicId= String.valueOf(topicId);
                                    classtipQuestion.question=question;
                                    classtipQuestion.questionType=questionType;
                                    classtipQuestion.groupNo= String.valueOf(groupNo);
                                    classtipQuestion.orderNo=orderNo;
                                    classtipQuestion.answerImage=answerImage;

                                    Common.questionYearRepository.insertQuestions(classtipQuestion);
                                    //Toast.makeText(QuestionYearActivity.this, new Gson().toJson(Common.questionYearRepository.countQuestions()), Toast.LENGTH_SHORT).show();
                                    Log.d("classtip23", new Gson().toJson(classtipQuestion));
                                }
                                else
                                {
                                   // Toast.makeText(QuestionYearActivity.this, "not", Toast.LENGTH_SHORT).show();
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }));
    }

    private void displayQuestionList(List<QuestionYear> questionYears) {
        QuestionYearAdapter adapter=new QuestionYearAdapter(this,questionYears);
        question_menu.setAdapter(adapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = ((TextView)view.findViewById(R.id.txt_id)).getText().toString();
        Toast.makeText(this, item, Toast.LENGTH_SHORT).show();
        if (isConnectingToInternet())
        loadQuestionList(item);
        else
            loadQuestionListDB(item);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(ChapterActivity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
