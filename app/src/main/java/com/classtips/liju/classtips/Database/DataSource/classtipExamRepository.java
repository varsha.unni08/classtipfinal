package com.classtips.liju.classtips.Database.DataSource;

import com.classtips.liju.classtips.Database.ModelDB.classtipExam;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/29/2018.
 */

public class classtipExamRepository implements IclasstipExamDataSource {

    private IclasstipExamDataSource iclasstipExamDataSource;
    private static classtipExamRepository instance;

    public classtipExamRepository(IclasstipExamDataSource iclasstipExamDataSource) {
        this.iclasstipExamDataSource = iclasstipExamDataSource;
    }

    public static classtipExamRepository getInstance(IclasstipExamDataSource iclasstipExamDataSource){
        if (instance==null)
            instance=new classtipExamRepository(iclasstipExamDataSource);
        return instance;
    }

    @Override
    public Flowable<List<classtipExam>> getExams() {
        return iclasstipExamDataSource.getExams();
    }

    @Override
    public Flowable<List<classtipExam>> getExamsById(int courseId, int subId) {
        return iclasstipExamDataSource.getExamsById(courseId,subId);
    }

    @Override
    public int isExist(int examId) {
        return iclasstipExamDataSource.isExist(examId);
    }

    @Override
    public int countExams() {
        return iclasstipExamDataSource.countExams();
    }

    @Override
    public void insertExams(classtipExam... classtipExams) {
        iclasstipExamDataSource.insertExams(classtipExams);
    }
}
