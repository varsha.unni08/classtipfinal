package com.classtips.liju.classtips.Database.Local;

import com.classtips.liju.classtips.Database.DataSource.IclasstipClassModeDataSource;
import com.classtips.liju.classtips.Database.ModelDB.classtipClassMode;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/24/2018.
 */

public class classtipClassModeDataSource implements IclasstipClassModeDataSource {

    private classtipClassModeDAO classModeDAO;
    private static classtipClassModeDataSource instance;

    public classtipClassModeDataSource(classtipClassModeDAO classModeDAO) {
        this.classModeDAO = classModeDAO;
    }

    public static classtipClassModeDataSource getInstance(classtipClassModeDAO classModeDAO){
        if (instance==null)
            instance=new classtipClassModeDataSource(classModeDAO);
        return instance;
    }

    @Override
    public Flowable<List<classtipClassMode>> getClassModes() {
        return classModeDAO.getClassModes();
    }

    @Override
    public int isExist(int classModeId) {
        return classModeDAO.isExist(classModeId);
    }

    @Override
    public int countClassModes() {
        return classModeDAO.countClassModes();
    }

    @Override
    public void emptyClassModes() {
        classModeDAO.emptyClassModes();
    }

    @Override
    public void insertClassModes(classtipClassMode... classtipClassModes) {
        classModeDAO.insertClassModes(classtipClassModes);
    }

    @Override
    public void updateClassmodes(classtipClassMode... classtipClassModes) {
        classModeDAO.updateClassmodes(classtipClassModes);
    }
}
