package com.classtips.liju.classtips.Database.DataSource;

import com.classtips.liju.classtips.Database.ModelDB.classtipQuestionYear;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/27/2018.
 */

public interface IclasstipQuestionYearDataSource {

    Flowable<List<classtipQuestionYear>> getQuestions();

    Flowable<List<classtipQuestionYear>> getQuestionsById(int examId);

    int isExist(int questionId);

    int countQuestions();

    void insertQuestions(classtipQuestionYear...classtipQuestions);

    void updateQuestions(classtipQuestionYear...classtipQuestionYears);
}
