package com.classtips.liju.classtips.Database.AdapterDB;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.classtips.liju.classtips.Database.ModelDB.classtipTopic;
import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.Utils.Common;
import com.classtips.liju.classtips.VideoHomeActivity;

import java.util.List;

public class FavoriteAdapterDB extends RecyclerView.Adapter<FavoriteAdapterDB.FavoriteViewHolder> {

    Context context;
    List<classtipTopic> classtipTopics;


    public FavoriteAdapterDB(Context context, List<classtipTopic> classtipTopics) {
        this.context = context;
        this.classtipTopics = classtipTopics;
    }

    @NonNull
    @Override
    public FavoriteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView= LayoutInflater.from(context).inflate(R.layout.fav_item_layout,null);
        return new FavoriteViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteViewHolder holder, final int i) {

        holder.text_topic_name.setText(classtipTopics.get(i).topic_name);
        holder.fav_sub_name.setText(classtipTopics.get(i).subName);
        holder.fav_chap_name.setText(classtipTopics.get(i).chapName);


        holder.setItemClickListner(new IItemClickListner() {
            @Override
            public void onClick(View v) {

                Common.currentTopic=null;
                Common.currentSubject=null;
                Common.currentClasstipSubject=null;

                Common.currentClasstipTopic=classtipTopics.get(i);
//                Toast.makeText(context, Common.currentSubject.subject_name, Toast.LENGTH_SHORT).show();
                context.startActivity(new Intent(context, VideoHomeActivity.class));
            }
        });

    }

    @Override
    public int getItemCount() {
        return classtipTopics.size();
    }

    public class FavoriteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView image_product;
        TextView text_topic_name,fav_sub_name,fav_chap_name;

        IItemClickListner itemClickListner;

        public void setItemClickListner(IItemClickListner itemClickListner) {
            this.itemClickListner = itemClickListner;
        }

        public FavoriteViewHolder(@NonNull View itemView) {
            super(itemView);
            image_product=(ImageView)itemView.findViewById(R.id.image_product);
            text_topic_name=(TextView)itemView.findViewById(R.id.txt_topic_name);
            fav_sub_name=(TextView)itemView.findViewById(R.id.fav_sub_name);
            fav_chap_name=(TextView)itemView.findViewById(R.id.fav_chap_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemClickListner.onClick(v);
        }
    }
}
