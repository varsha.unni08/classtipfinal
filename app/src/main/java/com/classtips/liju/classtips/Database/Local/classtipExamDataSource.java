package com.classtips.liju.classtips.Database.Local;

import com.classtips.liju.classtips.Database.DataSource.IclasstipExamDataSource;
import com.classtips.liju.classtips.Database.ModelDB.classtipExam;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/29/2018.
 */

public class classtipExamDataSource implements IclasstipExamDataSource {

    private classtipExamDAO examDAO;
    private static classtipExamDataSource instance;

    public classtipExamDataSource(classtipExamDAO examDAO) {
        this.examDAO = examDAO;
    }

    public static classtipExamDataSource getInstance(classtipExamDAO examDAO){
        if (instance==null)
            instance=new classtipExamDataSource(examDAO);
        return instance;
    }

    @Override
    public Flowable<List<classtipExam>> getExams() {
        return examDAO.getExams();
    }

    @Override
    public Flowable<List<classtipExam>> getExamsById(int courseId, int subId) {
        return examDAO.getExamsById(courseId,subId);
    }

    @Override
    public int isExist(int examId) {
        return examDAO.isExist(examId);
    }

    @Override
    public int countExams() {
        return examDAO.countExams();
    }

    @Override
    public void insertExams(classtipExam... classtipExams) {
        examDAO.insertExams(classtipExams);
    }
}
