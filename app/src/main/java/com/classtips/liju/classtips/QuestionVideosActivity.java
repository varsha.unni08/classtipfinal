package com.classtips.liju.classtips;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.classtips.liju.classtips.Database.ModelDB.classtipQuestion;
import com.classtips.liju.classtips.Downloading.EncryptDecryptUtils;
import com.classtips.liju.classtips.Downloading.FileUtils;
import com.classtips.liju.classtips.Downloading.Utils;
import com.classtips.liju.classtips.Model.CheckPurchaseResponse;
import com.classtips.liju.classtips.Model.QuestionVideo;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuestionVideosActivity extends AppCompatActivity {
    IClassTipAPI mService;
    VideoView videoView;
    Button btn_topic,next_topic;
    int orderNo=1;
    private int position = 0;
    String key;

    String phone;
    String subject_id;
    String topic_id;
    String name;

    private MediaController mediaController;
    CompositeDisposable compositeDisposable=new CompositeDisposable();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_videos);

        videoView = (VideoView)findViewById(R.id.videoView);
        mService= Common.getAPI();
        if (mediaController == null) {
            mediaController = new MediaController(QuestionVideosActivity.this);

            // Set the videoView that acts as the anchor for the MediaController.
            mediaController.setAnchorView(videoView);


            // Set MediaController for VideoView
            videoView.setMediaController(mediaController);
        }

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            public void onPrepared(MediaPlayer mediaPlayer) {


                videoView.seekTo(position);
                if (position == 0) {
                    videoView.start();
                }

                // When video Screen change size.
                mediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {

                        // Re-Set the videoView that acts as the anchor for the MediaController
                        mediaController.setAnchorView(videoView);
                    }
                });
            }
        });
        next_topic=(Button)findViewById(R.id.btn_next_topic) ;
        btn_topic=(Button)findViewById(R.id.btn_topic) ;

        key=getIntent().getStringExtra("key");
        if (key==null){
            key="0";
        }

        next_topic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoView.stopPlayback();
                startActivity(new Intent(QuestionVideosActivity.this,TopicActivity.class));
                finish();


            }
        });
        btn_topic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoView.stopPlayback();
                playQuestionVideo();

            }
        });
        playQuestionVideo();
    }

    private void playQuestionVideo() {
        if (isConnectingToInternet()) {
            SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
            SharedPreferences.Editor editor = pref.edit();
            try {
                phone=Common.currentUser.getPhone();
            }catch (NullPointerException e){
                phone=pref.getString("phone", null);

            }
            try {
                name=Common.currentUser.getName();
            }catch (NullPointerException e){
                name=pref.getString("name", null);
            }
            editor.commit();
            try {
                subject_id=Common.currentSubject.subject_id;
            }catch (NullPointerException e){
                subject_id=Common.currentClasstipSubject.subject_id;
            }
            try {
                topic_id=Common.currentTopic.topic_id;
            }catch (NullPointerException e){
                topic_id=Common.currentClasstipTopic.topic_id;
            }

            mService.checkPurchaseExists(phone, subject_id)
                    .enqueue(new Callback<CheckPurchaseResponse>() {
                        @Override
                        public void onResponse(Call<CheckPurchaseResponse> call, Response<CheckPurchaseResponse> response) {
                            CheckPurchaseResponse purchaseResponse = response.body();

                            if (purchaseResponse.isExists()) {

                                mService.getQuestionVideos(topic_id, String.valueOf(orderNo))
                                        .enqueue(new Callback<QuestionVideo>() {
                                            @Override
                                            public void onResponse(Call<QuestionVideo> call, Response<QuestionVideo> response) {
                                                final QuestionVideo video = response.body();

                                                if (video.getQuestionvideo()==null){
                                                    videoView.stopPlayback();
                                                    Toast.makeText(QuestionVideosActivity.this, "There Is No More Question For This Section", Toast.LENGTH_SHORT).show();
                                                    startActivity(new Intent(QuestionVideosActivity.this,TopicActivity.class));
                                                    finish();
                                                }else{
                                                    videoView.setVideoPath(video.getQuestionvideo());
                                                    videoView.start();
                                                    orderNo += 1;

                                                    String id= video.getQuestionid();
                                                    String examId= video.getExamid();
                                                    String topicId= video.getTopicid();
                                                    String question = video.getQuestion();
                                                    String questionType =video.getQuestiontype();
                                                    String questionVideo=video.getQuestionvideo();
                                                    String orderNo=video.getOrderno();

                                                    if (Common.questionRepository.isExist(Integer.parseInt(id))!=1)
                                                    {

                                                        classtipQuestion classtipQuestion =new classtipQuestion();
                                                        classtipQuestion.question_id= id;
                                                        classtipQuestion.question_video= questionVideo;
                                                        classtipQuestion.exam_id= examId;
                                                        classtipQuestion.topic_id= topicId;
                                                        classtipQuestion.question_img=question;
                                                        classtipQuestion.question_type=questionType;
                                                        classtipQuestion.order_no= orderNo;


                                                        Common.questionRepository.insertQuestions(classtipQuestion);
                                                        Toast.makeText(QuestionVideosActivity.this, new Gson().toJson(Common.questionRepository.countQuestions()), Toast.LENGTH_SHORT).show();
                                                        Log.d("classtip23", new Gson().toJson(classtipQuestion));
                                                    }
                                                    else
                                                    {
                                                        Toast.makeText(QuestionVideosActivity.this, "not", Toast.LENGTH_SHORT).show();
                                                    }
                                                }


                                            }

                                            @Override
                                            public void onFailure(Call<QuestionVideo> call, Throwable t) {
                                                Toast.makeText(QuestionVideosActivity.this, "no video", Toast.LENGTH_SHORT).show();
                                            }
                                        });

                            }
                        }

                        @Override
                        public void onFailure(Call<CheckPurchaseResponse> call, Throwable t) {

                        }
                    });
        }else {


                try {
                    topic_id=Common.currentClasstipTopic.topic_id;
                }catch (NullPointerException e){

                    topic_id=Common.currentTopic.topic_id;
                }

                    compositeDisposable.add(Common.questionRepository.getQuestionsById(Integer.parseInt(topic_id),orderNo)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribeOn(Schedulers.io())
                            .subscribe(new Consumer<List<classtipQuestion>>() {
                                           @Override
                                           public void accept(List<classtipQuestion> classtipQuestions) throws Exception {

                                               JSONArray req = new JSONArray(new Gson().toJson(classtipQuestions));
                                               //Toast.makeText(QuestionVideosActivity.this, "id:", Toast.LENGTH_SHORT).show();
                                               if (req.length()==0){
                                                   videoView.stopPlayback();
                                                   Toast.makeText(QuestionVideosActivity.this, "You completed the question of this section", Toast.LENGTH_SHORT).show();
                                                   startActivity(new Intent(QuestionVideosActivity.this,TopicActivity.class));
                                                   finish();
                                               }
                                               for (int i = 0; i < req.length(); ++i) {
                                                   JSONObject rec = req.getJSONObject(i);
                                                   int id = rec.getInt("id");
                                                   String url = rec.getString("questionVideo");
                                                   Toast.makeText(QuestionVideosActivity.this, "id:"+url, Toast.LENGTH_SHORT).show();


                                                       String downloadFileName = url.replace(Utils.mainUrl, "");//Create file name by picking download file name from URL
                                                       Uri uri = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                                                               + "/" + Utils.downloadDirectory + "/" + downloadFileName);

                                                   if (downloadFileName.indexOf(".") > 0)
                                                       downloadFileName = downloadFileName.substring(0, downloadFileName.lastIndexOf("."));
                                                   Uri ur = Uri.parse(Environment.getExternalStorageDirectory().getPath()
                                                           + "/" + Utils.downloadDirectory + "/" + downloadFileName);
                                                   try {
                                                       playAudio(FileUtils.getTempFileDescriptor(QuestionVideosActivity.this, decrypt(ur, uri)));
                                                   } catch (IOException |NullPointerException e) {
                                                       Toast.makeText(QuestionVideosActivity.this, "You yet havent download this video", Toast.LENGTH_SHORT).show();
                                                       e.printStackTrace();
                                                   }


                                               }
                                           }
                                       }
                            ));

        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        RelativeLayout.LayoutParams params= (RelativeLayout.LayoutParams) videoView.getLayoutParams();


        final View decorView = getWindow().getDecorView();
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            //Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
            btn_topic.setVisibility(View.GONE);
            next_topic.setVisibility(View.GONE);
            decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                @Override
                public void onSystemUiVisibilityChange(int visibility) {
                    if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                        // TODO: The system bars are visible. Make any desired
                        // adjustments to your UI, such as showing the action bar or
                        // other navigational controls.
                        decorView.setSystemUiVisibility(
                                View.SYSTEM_UI_FLAG_IMMERSIVE
                                        // Set the content to appear under the system bars so that the
                                        // content doesn't resize when the system bars hide and show.
                                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                        // Hide the nav bar and status bar
                                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
                    } else {
                        // TODO: The system bars are NOT visible. Make any desired
                        // adjustments to your UI, such as hiding the action bar or
                        // other navigational controls.
                    }
                }
            });
//            requestWindowFeature(Window.FEATURE_NO_TITLE);
//            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN);


            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_IMMERSIVE
                            // Set the content to appear under the system bars so that the
                            // content doesn't resize when the system bars hide and show.
                            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            // Hide the nav bar and status bar
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            //Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();


            btn_topic.setVisibility(View.VISIBLE);
            next_topic.setVisibility(View.VISIBLE);
//            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);

            decorView.setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                @Override
                public void onSystemUiVisibilityChange(int visibility) {
                    if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                    } else {
                    }
                }
            });

            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            |View.SYSTEM_UI_FLAG_VISIBLE);
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT,0);
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP,0);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,0);
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,0);
        }
        videoView.setLayoutParams(params);


    }

    private byte[] decrypt(Uri ur, Uri uri) {
        try {
//            Toast.makeText(this, "decrypting", Toast.LENGTH_SHORT).show();
            byte[] fileData = FileUtils.readFile(String.valueOf(ur));
            byte[] decryptedBytes = EncryptDecryptUtils.decode(EncryptDecryptUtils.getInstance(this).getSecretKey(), fileData);
//            FileUtils.saveFile(decryptedBytes, String.valueOf(uri));
            return decryptedBytes;
        } catch (Exception e) {
        }
        return null;
    }



    private void playAudio(File fileDescriptor) {
        if (null == fileDescriptor) {
            return;
        }
        play(fileDescriptor);
    }
    public void play(final File fileDescriptor) {

        try {
            Uri video = Uri.parse(String.valueOf(fileDescriptor));
            Toast.makeText(QuestionVideosActivity.this, String.valueOf(fileDescriptor), Toast.LENGTH_SHORT).show();
            videoView.setVideoURI(video);
            videoView.start();
            orderNo+=1;
//            Toast.makeText(QuestionVideosActivity.this, "Orderno:" + orderNo, Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
        }
    }

    private boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(ChapterActivity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, "key"+key, Toast.LENGTH_SHORT).show();
            if (key=="0"){
                startActivity(new Intent(QuestionVideosActivity.this, VideoHomeActivity.class));
                finish();
            }else{
                startActivity(new Intent(QuestionVideosActivity.this, TopicActivity.class));
                finish();
            }

    }

    @Override
    protected void onPause() {
        super.onPause();
        videoView.pause();
    }
}
