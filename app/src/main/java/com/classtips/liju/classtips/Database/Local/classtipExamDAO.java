package com.classtips.liju.classtips.Database.Local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.classtips.liju.classtips.Database.ModelDB.classtipExam;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/29/2018.
 */
@Dao
public interface classtipExamDAO {

    @Query("SELECT * FROM classtipExam")
    Flowable<List<classtipExam>> getExams();

    @Query("SELECT * FROM classtipExam where courseId=:courseId and subId=:subId")
    Flowable<List<classtipExam>> getExamsById(int courseId,int subId);

    @Query("SELECT EXISTS(SELECT 1 FROM classtipExam where id=:examId)")
    int isExist(int examId);

    @Query("SELECT COUNT(*) FROM classtipExam")
    int countExams();

    @Insert
    void insertExams(classtipExam...classtipExams);
}
