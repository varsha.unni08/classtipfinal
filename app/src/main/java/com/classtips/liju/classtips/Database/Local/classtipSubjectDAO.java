package com.classtips.liju.classtips.Database.Local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.classtips.liju.classtips.Database.ModelDB.classtipSubject;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/7/2018.
 */
@Dao
public interface classtipSubjectDAO {

    @Query("SELECT * FROM classtipSubject")
    Flowable<List<classtipSubject>> getSubjects();

    @Query("SELECT * FROM classtipSubject where course_id=:courseId")
    Flowable<List<classtipSubject>> getSubjectsById(int courseId);

    @Query("SELECT EXISTS(SELECT 1 FROM classtipSubject where subject_id=:subId)")
    int isExist(int subId);

    @Query("SELECT COUNT(*) FROM classtipSubject")
    int countSubjects();

    @Query("Delete  from classtipSubject")
    void  emptySubjects();

    @Insert
    void insertSubjects(classtipSubject...classtipSubjects);

    @Update
    void updateSubjects(classtipSubject...classtipSubjects);

    @Delete
    void deleteSubjects(classtipSubject...classtipSubjects);
}
