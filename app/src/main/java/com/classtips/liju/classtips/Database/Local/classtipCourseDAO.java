package com.classtips.liju.classtips.Database.Local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.classtips.liju.classtips.Database.ModelDB.classtipCourse;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/24/2018.
 */
@Dao
public interface classtipCourseDAO {

    @Query("SELECT * FROM classtipCourse")
    Flowable<List<classtipCourse>> getCourses();

    @Query("SELECT * FROM classtipCourse where id=:courseId")
    Flowable<List<classtipCourse>> getCoursesById(int courseId);

    @Query("SELECT EXISTS(SELECT 1 FROM classtipCourse where id=:courseId)")
    int isExist(int courseId);

    @Query("SELECT COUNT(*) FROM classtipCourse")
    int countCourses();

    @Insert
    void insertCourses(classtipCourse...classtipCourses);


}
