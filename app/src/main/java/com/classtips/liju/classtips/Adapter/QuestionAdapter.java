package com.classtips.liju.classtips.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.Model.CheckPurchaseResponse;
import com.classtips.liju.classtips.Model.Question;
import com.classtips.liju.classtips.QuestionVideoActivity;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by User on 8/22/2018.
 */

public class QuestionAdapter extends RecyclerView.Adapter<QuestionViewHolder>{
    Context context;
    List<Question> questions;
    IClassTipAPI mService;

    String phone,subject_id,topicType;

    public QuestionAdapter(Context context, List<Question> questions) {
        this.context = context;
        this.questions = questions;
    }

    @NonNull
    @Override
    public QuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(context).inflate(R.layout.question_item_layout,null);
        return new QuestionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final QuestionViewHolder holder, final int position) {
        mService= Common.getAPI();
        Picasso.with(context)
                .load(questions.get(position).question_img)
                .into(holder.image_product);
        //holder.text_topic_name.setText(questions.get(position).question);
        Toast.makeText(context, questions.get(position).question_img, Toast.LENGTH_SHORT).show();

        holder.text_watch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.text_watch.setEnabled(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        holder.text_watch.setEnabled(true);
                    }
                },3000);
                holder.text_watch.setBackgroundResource(R.drawable.text_round);
                Common.currentQuestion=questions.get(position);
                Common.currentClasstipQuestion=null;

                try {
                    phone=Common.currentUser.getPhone();
                }catch (NullPointerException e){
                    SharedPreferences pref = context.getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    //Toast.makeText(this, "testing..."+pref.getString("phone", null), Toast.LENGTH_SHORT).show();
                    phone=pref.getString("phone", null);
                    editor.commit();
                }
                try{
                    try {
                        subject_id=Common.currentClasstipTopic.subId;
                    }catch (NullPointerException e){
                        subject_id=Common.currentSubject.subject_id;
                    }
                }catch (Exception e){
                    subject_id=Common.currentClasstipSubject.subject_id;
                }
                try{
                    topicType=Common.currentTopic.topic_type;
                }catch (NullPointerException e){
                    topicType=Common.currentClasstipTopic.topic_type;
                }

                mService.checkPurchaseExists(phone,subject_id)
                        .enqueue(new Callback<CheckPurchaseResponse>() {
                            @Override
                            public void onResponse(Call<CheckPurchaseResponse> call, Response<CheckPurchaseResponse> response) {
                                CheckPurchaseResponse purchaseResponse=response.body();
                                if (purchaseResponse.isExists()){
//                                    Toast.makeText(context, "Exists", Toast.LENGTH_SHORT).show();
                                    context.startActivity(new Intent(context,QuestionVideoActivity.class));
                                }
                                else
                                {
//                                    Toast.makeText(context, "not", Toast.LENGTH_SHORT).show();
                                    if (topicType.equals("P"))
                                    {
                                        Toast.makeText(context,"you need to purchase", Toast.LENGTH_SHORT).show();
                                    }

                                    else
                                    {
//                                        Toast.makeText(context,"clicked", Toast.LENGTH_SHORT).show();
                                        context.startActivity(new Intent(context,QuestionVideoActivity.class ));
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<CheckPurchaseResponse> call, Throwable t) {

                            }
                        });



            }
        });

        holder.text_answer_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(context, "clicked", Toast.LENGTH_SHORT).show();

                AlertDialog.Builder answerView = new AlertDialog.Builder(context);
                LayoutInflater factory = LayoutInflater.from(v.getRootView().getContext());
                final View view = factory.inflate(R.layout.question_answer_layout, null);
                ImageView img_answer;
                img_answer=(ImageView)view.findViewById(R.id.img_answer);

                try{
                    Picasso.with(context)
                            .load(questions.get(position).answer_img)
                            .into(img_answer);
                }catch (Exception e){

                }

                answerView.setView(view);
                answerView.show();
//
            }
        });

        holder.image_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder zoomView = new AlertDialog.Builder(context);
                LayoutInflater factory = LayoutInflater.from(v.getRootView().getContext());
                final View view = factory.inflate(R.layout.zooming_layout, null);
//                ImageView image_product;
                PhotoView photoView;
                photoView=(PhotoView)view.findViewById(R.id.photo_view);
//                image_product=(ImageView)view.findViewById(R.id.image_product);
//                URL newurl=null;
//                try {
//                    newurl = new URL(questions.get(position).question_img);
//                } catch (MalformedURLException e) {
//                    e.printStackTrace();
//                }
//                try {
//                    Bitmap mIcon_val = BitmapFactory.decodeStream(newurl.openConnection() .getInputStream());
//                    photoView.setImageBitmap(mIcon_val);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
                Picasso.with(context)
                        .load(questions.get(position).question_img)
                        .into(photoView);
                zoomView.setView(view);

                zoomView.show();
//                Intent intent=new Intent(context,ZoomingActivity.class);
//                intent.putExtra("name",questions.get(position).question_img);
//                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return questions.size();
    }
}
