package com.classtips.liju.classtips.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.Model.CheckPurchaseResponse;
import com.classtips.liju.classtips.Model.QuestionYear;
import com.classtips.liju.classtips.QuestionVideoActivity;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by User on 9/13/2018.
 */

public class QuestionYearAdapter extends RecyclerView.Adapter<QuestionYearViewHolder> {
    Context context;
    List<QuestionYear> questions;
    IClassTipAPI mService;
    String phone,subject_id;

    public QuestionYearAdapter(Context context, List<QuestionYear> questions) {
        this.context = context;
        this.questions = questions;
    }

    @Override
    public QuestionYearViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(context).inflate(R.layout.question_year_item_layout,null);
        return new QuestionYearViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(QuestionYearViewHolder holder, final int position) {

        mService= Common.getAPI();
               Picasso.with(context)
                .load(questions.get(position).question_img)
                .into(holder.image_product);

        holder.text_watch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.currentQuestionYear=questions.get(position);
                Common.currentClasstipQuestionYear=null;
                try {
                    phone=Common.currentUser.getPhone();
                }catch (NullPointerException e){
                    SharedPreferences pref = context.getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    //Toast.makeText(this, "testing..."+pref.getString("phone", null), Toast.LENGTH_SHORT).show();
                    phone=pref.getString("phone", null);
                    editor.commit();
                }
                try {
                    subject_id=Common.currentSubject.subject_id;
                }catch (NullPointerException e){
                    subject_id=Common.currentClasstipSubject.subject_id;
                }
                mService.checkPurchaseExists(phone,subject_id)
                        .enqueue(new Callback<CheckPurchaseResponse>() {
                            @Override
                            public void onResponse(Call<CheckPurchaseResponse> call, Response<CheckPurchaseResponse> response) {
                                CheckPurchaseResponse purchaseResponse=response.body();
                                if (purchaseResponse.isExists()){
                                    Toast.makeText(context, "Exists", Toast.LENGTH_SHORT).show();
                                    Intent intent=new Intent(context,QuestionVideoActivity.class);
                                    intent.putExtra("key","1");
                                    context.startActivity(intent);
                                }
                                else
                                {
                                    Toast.makeText(context, "not", Toast.LENGTH_SHORT).show();
                                    if (Common.currentQuestionYear.question_type.equals("p"))
                                    {
                                        Toast.makeText(context,"you need to purchase", Toast.LENGTH_SHORT).show();
                                    }

                                    else
                                    {
                                        Toast.makeText(context,"clicked", Toast.LENGTH_SHORT).show();
                                        Intent intent=new Intent(context,QuestionVideoActivity.class);
                                        intent.putExtra("key","1");
                                        context.startActivity(intent);
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<CheckPurchaseResponse> call, Throwable t) {

                            }
                        });
            }
        });

        holder.text_answer_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder answerView = new AlertDialog.Builder(context);
                LayoutInflater factory = LayoutInflater.from(v.getRootView().getContext());
                final View view = factory.inflate(R.layout.question_answer_layout, null);
                ImageView img_answer;
                img_answer=(ImageView)view.findViewById(R.id.img_answer);

                try{
                    Picasso.with(context)
                            .load(questions.get(position).answer_img)
                            .into(img_answer);
                }catch (Exception e){

                }

                answerView.setView(view);
                answerView.show();
//
            }
        });
        holder.image_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder zoomView = new AlertDialog.Builder(context);
                LayoutInflater factory = LayoutInflater.from(v.getRootView().getContext());
                final View view = factory.inflate(R.layout.zooming_layout, null);
//                ImageView image_product;
                PhotoView photoView;
                photoView=(PhotoView)view.findViewById(R.id.photo_view);
//                image_product=(ImageView)view.findViewById(R.id.image_product);
//                URL newurl=null;
//                try {
//                    newurl = new URL(questions.get(position).question_img);
//                } catch (MalformedURLException e) {
//                    e.printStackTrace();
//                }
//                try {
//                    Bitmap mIcon_val = BitmapFactory.decodeStream(newurl.openConnection() .getInputStream());
//                    photoView.setImageBitmap(mIcon_val);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
                Picasso.with(context)
                        .load(questions.get(position).question_img)
                        .into(photoView);
                zoomView.setView(view);

                zoomView.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return questions.size();
    }
}
