package com.classtips.liju.classtips.Database.DataSource;

import com.classtips.liju.classtips.Database.ModelDB.classtipTopic;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/8/2018.
 */

public class classtipTopicRepository implements IclasstipTopicDataSource {

    private IclasstipTopicDataSource iclasstipTopicDataSource;
    private static classtipTopicRepository instance;

    public classtipTopicRepository(IclasstipTopicDataSource iclasstipTopicDataSource) {
        this.iclasstipTopicDataSource = iclasstipTopicDataSource;
    }

    public static classtipTopicRepository getInstance(IclasstipTopicDataSource iclasstipTopicDataSource){
        if (instance==null)
            instance=new classtipTopicRepository(iclasstipTopicDataSource);
        return instance;
    }

    @Override
    public Flowable<List<classtipTopic>> getTopics() {
        return iclasstipTopicDataSource.getTopics();
    }

    @Override
    public Flowable<List<classtipTopic>> getTopicsById(int chapId, String classmodeId) {
        return iclasstipTopicDataSource.getTopicsById(chapId, classmodeId);
    }


    @Override
    public Flowable<List<classtipTopic>> getFavoritessById() {
        return iclasstipTopicDataSource.getFavoritessById();
    }

    @Override
    public int isExist(int topicId) {
        return iclasstipTopicDataSource.isExist(topicId);
    }

    @Override
    public int isViewed(String flag,int topicId) {
        return iclasstipTopicDataSource.isViewed(flag,topicId);
    }

    @Override
    public int isUpdated(String flag, int topicId) {
        return iclasstipTopicDataSource.isUpdated(flag, topicId);
    }

    @Override
    public int isFavorite(int topicId) {
        return iclasstipTopicDataSource.isFavorite(topicId);
    }

    @Override
    public int countTopics() {
        return iclasstipTopicDataSource.countTopics();
    }

    @Override
    public void insertTopics(classtipTopic... classtipTopics) {
        iclasstipTopicDataSource.insertTopics(classtipTopics);
    }

    @Override
    public void updateTopicsById(String subId, String subName, String chapName, String flag, int topicId) {
        iclasstipTopicDataSource.updateTopicsById(subId, subName, chapName, flag, topicId);
    }

    @Override
    public void updateTopics(String flag, int topicId) {
        iclasstipTopicDataSource.updateTopics(flag,topicId);
    }

    @Override
    public void updateFavorite(int topicId) {
        iclasstipTopicDataSource.updateFavorite(topicId);
    }

    @Override
    public void removeFavorite(int topicId) {
        iclasstipTopicDataSource.removeFavorite(topicId);
    }

    @Override
    public void updateclasstipTopics(classtipTopic... classtipTopics) {
        iclasstipTopicDataSource.updateclasstipTopics(classtipTopics);
    }
}
