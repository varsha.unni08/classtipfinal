package com.classtips.liju.classtips.Utils;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import com.classtips.liju.classtips.R;
import com.wang.avi.AVLoadingIndicatorView;
import com.wang.avi.indicators.BallSpinFadeLoaderIndicator;

public class ProgressDialog {
    private Context context;
    private android.app.Dialog progressDialog;

    public ProgressDialog(Context context){
        this.context = context;
    }

    public void show(){
        progressDialog = new android.app.Dialog(context);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCancelable(false);
        progressDialog.show();

        AVLoadingIndicatorView progressLoading =  progressDialog.findViewById(R.id.progressLoading);
        BallSpinFadeLoaderIndicator ballSpinFadeLoaderIndicator = new BallSpinFadeLoaderIndicator();
        progressLoading.setIndicator(ballSpinFadeLoaderIndicator);
        progressLoading.smoothToShow();
    }


    public void dismiss(){
        if (progressDialog != null) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }
}
