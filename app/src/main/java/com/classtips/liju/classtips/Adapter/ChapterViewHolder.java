package com.classtips.liju.classtips.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.R;

/**
 * Created by User on 8/14/2018.
 */

public class ChapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView txt_chapter_name;



    IItemClickListner itemClickListner;

    public void setItemClickListner(IItemClickListner itemClickListner) {
        this.itemClickListner = itemClickListner;
    }

    public ChapterViewHolder(View itemView) {
        super(itemView);


            txt_chapter_name=(TextView)itemView.findViewById(R.id.txt_chapter_name);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        itemClickListner.onClick(v);
    }
}
