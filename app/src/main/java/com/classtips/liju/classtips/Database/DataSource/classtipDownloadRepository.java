package com.classtips.liju.classtips.Database.DataSource;

import com.classtips.liju.classtips.Database.ModelDB.classtipDownload;

/**
 * Created by User on 10/26/2018.
 */

public class classtipDownloadRepository implements IclasstipDownloadDataSource {

    private IclasstipDownloadDataSource iclasstipDownloadDataSource;
    private static classtipDownloadRepository instance;

    public classtipDownloadRepository(IclasstipDownloadDataSource iclasstipDownloadDataSource) {
        this.iclasstipDownloadDataSource = iclasstipDownloadDataSource;
    }

    public static classtipDownloadRepository getInstance(IclasstipDownloadDataSource iclasstipDownloadDataSource){
        if (instance==null)
            instance=new classtipDownloadRepository(iclasstipDownloadDataSource);
        return instance;
    }

    @Override
    public int isExist(int downloadId) {
        return iclasstipDownloadDataSource.isExist(downloadId);
    }

    @Override
    public int isFlgExist(String flag, int downloadId) {
        return iclasstipDownloadDataSource.isFlgExist(flag,downloadId);
    }

    @Override
    public int countSubjects() {
        return iclasstipDownloadDataSource.countSubjects();
    }

    @Override
    public void updateDownloads(String flag, int downloadId) {
        iclasstipDownloadDataSource.updateDownloads(flag,downloadId);
    }

    @Override
    public void emptyDownloads() {
        iclasstipDownloadDataSource.emptyDownloads();
    }

    @Override
    public void insertDownloads(classtipDownload... classtipDownloads) {
        iclasstipDownloadDataSource.insertDownloads(classtipDownloads);
    }

    @Override
    public void updateDownloads(classtipDownload... classtipDownloads) {
        iclasstipDownloadDataSource.updateDownloads(classtipDownloads);
    }

    @Override
    public void deleteDownloads(classtipDownload... classtipDownloads) {
        iclasstipDownloadDataSource.deleteDownloads(classtipDownloads);
    }
}
