package com.classtips.liju.classtips.Database.ModelDB;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by User on 10/26/2018.
 */
@Entity(tableName ="classtipDownload")
public class classtipDownload {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name="id")
    public String id;

    @ColumnInfo(name="fileName")
    public String fileName;

    @ColumnInfo(name="courseId")
    public String courseId;

    @ColumnInfo(name="subId")
    public String subId;

    @ColumnInfo(name="downloadFlag")
    public String downloadFlag;
}
