package com.classtips.liju.classtips.Database.Local;

import com.classtips.liju.classtips.Database.DataSource.IclasstipDownloadDataSource;
import com.classtips.liju.classtips.Database.ModelDB.classtipDownload;

/**
 * Created by User on 10/26/2018.
 */

public class classtipDownloadDataSource implements IclasstipDownloadDataSource {

    private classtipDownloadDAO downloadDAO;
    private static classtipDownloadDataSource instance;

    public classtipDownloadDataSource(classtipDownloadDAO downloadDAO) {
        this.downloadDAO = downloadDAO;
    }
    public static classtipDownloadDataSource getInstance(classtipDownloadDAO downloadDAO){
        if(instance==null)
            instance=new classtipDownloadDataSource(downloadDAO);
        return instance;
    }

    @Override
    public int isExist(int downloadId) {
        return downloadDAO.isExist(downloadId);
    }

    @Override
    public int isFlgExist(String flag, int downloadId) {
        return downloadDAO.isFlgExist(flag,downloadId);
    }

    @Override
    public int countSubjects() {
        return downloadDAO.countSubjects();
    }

    @Override
    public void updateDownloads(String flag, int downloadId) {
        downloadDAO.updateDownloads(flag,downloadId);
    }

    @Override
    public void emptyDownloads() {
        downloadDAO.emptyDownloads();
    }

    @Override
    public void insertDownloads(classtipDownload... classtipDownloads) {
        downloadDAO.insertDownloads(classtipDownloads);
    }

    @Override
    public void updateDownloads(classtipDownload... classtipDownloads) {
        downloadDAO.updateDownloads(classtipDownloads);
    }

    @Override
    public void deleteDownloads(classtipDownload... classtipDownloads) {
        downloadDAO.deleteDownloads(classtipDownloads);
    }
}
