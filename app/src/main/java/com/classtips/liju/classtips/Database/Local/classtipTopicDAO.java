package com.classtips.liju.classtips.Database.Local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.classtips.liju.classtips.Database.ModelDB.classtipTopic;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/8/2018.
 */
@Dao
public interface classtipTopicDAO {
    @Query("SELECT * FROM classtipTopic")
    Flowable<List<classtipTopic>> getTopics();

    @Query("SELECT * FROM classtipTopic where chptr_id=:chapId AND classmode_id=:classmodeId")
    Flowable<List<classtipTopic>> getTopicsById(int chapId,String classmodeId);

    @Query("SELECT EXISTS(SELECT 1 FROM classtipTopic where topic_id=:topicId)")
    int isExist(int topicId);

    @Query("SELECT EXISTS(SELECT 1 FROM classtipTopic where viewFlag=:flag and topic_id=:topicId)")
    int isViewed(String flag,int topicId);

    @Query("SELECT EXISTS(SELECT 1 FROM classtipTopic where updtn_flag=:flag and topic_id=:topicId)")
    int isUpdated(String flag,int topicId);

    @Query("SELECT EXISTS(SELECT 1 FROM classtipTopic where favId=1 and topic_id=:topicId)")
    int isFavorite(int topicId);

    @Query("SELECT COUNT(*) FROM classtipTopic")
    int countTopics();

    @Query("SELECT * FROM classtipTopic where favId='1' order by subId asc,chptr_id asc")
    Flowable<List<classtipTopic>> getFavoritessById();

    @Insert
    void insertTopics(classtipTopic...classtipTopics);

    @Query("UPDATE classtipTopic SET subId=:subId,subName=:subName,chapName=:chapName,updtn_flag=:flag WHERE topic_id = :topicId")
    void updateTopicsById(String subId,String subName,String chapName,String flag, int topicId);

    @Query("UPDATE classtipTopic SET viewFlag=:flag WHERE topic_id = :topicId")
    void updateTopics(String flag, int topicId);

    @Query("UPDATE classtipTopic SET favId=1 WHERE topic_id = :topicId")
    void updateFavorite(int topicId);

    @Query("UPDATE classtipTopic SET favId=0 WHERE topic_id = :topicId")
    void removeFavorite(int topicId);

    @Update
    void updateclasstipTopics(classtipTopic...classtipTopics);
}
