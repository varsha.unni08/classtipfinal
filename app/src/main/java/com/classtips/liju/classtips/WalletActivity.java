package com.classtips.liju.classtips;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.classtips.liju.classtips.Database.ModelDB.classtipUser;
import com.classtips.liju.classtips.Model.Wallet;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletActivity extends AppCompatActivity {
    Button btn_update;
    TextView text_wallet;
    String phone;
    CompositeDisposable compositeDisposable;

    IClassTipAPI mService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

        btn_update=(Button)findViewById(R.id.btn_update) ;
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(WalletActivity.this,QRcodeActivity.class);
                intent.putExtra("key","3");
                startActivity(intent);
                finish();
            }
        });

        compositeDisposable=new CompositeDisposable();
        mService= Common.getAPI();
        text_wallet=(TextView)findViewById(R.id.txt_wallet) ;

        if (isConnectingToInternet()) {
            compositeDisposable.add(Common.userRepository.getUsers()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Consumer<List<classtipUser>>() {
                        @Override
                        public void accept(List<classtipUser> classtipUsers) throws Exception {
                            Log.d("classtip234", new Gson().toJson(classtipUsers));
                            try {
                                JSONArray req = new JSONArray(new Gson().toJson(classtipUsers));
                                for (int i = 0; i < req.length(); ++i) {
                                    JSONObject rec = req.getJSONObject(i);
                                    phone = rec.getString("phone");
                                }
                                mService.getWalletInformation(phone)
                                        .enqueue(new Callback<Wallet>() {
                                            @Override
                                            public void onResponse(Call<Wallet> call, Response<Wallet> response) {
                                                Wallet wallet = response.body();
                                                text_wallet.setText(new StringBuilder("₹").append(wallet.getWalletmoney()));
                                            }

                                            @Override
                                            public void onFailure(Call<Wallet> call, Throwable t) {

                                            }
                                        });
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }));

        }else {
            Toast.makeText(this, "Oops!!! No internet", Toast.LENGTH_LONG).show();
        }
    }

    private boolean isConnectingToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(ChapterActivity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager
                .getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }
    @Override
    public void onBackPressed() {
       finish();
    }
}
