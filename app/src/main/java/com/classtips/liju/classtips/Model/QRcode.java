package com.classtips.liju.classtips.Model;

/**
 * Created by User on 9/16/2018.
 */

public class QRcode {
    private String qrprice;
    private boolean updated;
    private boolean updateqrcode;
    private String error_msg;
    private String walletmoney;

    public QRcode() {
    }

    public String getQrprice() {
        return qrprice;
    }

    public void setQrprice(String qrprice) {
        this.qrprice = qrprice;
    }

    public String getWalletmoney() {
        return walletmoney;
    }

    public void setWalletmoney(String walletmoney) {
        this.walletmoney = walletmoney;
    }

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }

    public boolean isUpdateqrcode() {
        return updateqrcode;
    }

    public void setUpdateqrcode(boolean updateqrcode) {
        this.updateqrcode = updateqrcode;
    }

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }
}
