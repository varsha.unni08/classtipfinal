package com.classtips.liju.classtips.Database.DataSource;

import com.classtips.liju.classtips.Database.ModelDB.classtipQuestion;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/27/2018.
 */

public class classtipQuestionRepository implements IclasstipQuestionDataSource {

    private IclasstipQuestionDataSource iclasstipQuestionDataSource;
    private static classtipQuestionRepository instance;

    public classtipQuestionRepository(IclasstipQuestionDataSource iclasstipQuestionDataSource) {
        this.iclasstipQuestionDataSource = iclasstipQuestionDataSource;
    }

    public static classtipQuestionRepository getInstance(IclasstipQuestionDataSource iclasstipQuestionDataSource){
        if (instance==null)
            instance=new classtipQuestionRepository(iclasstipQuestionDataSource);
        return instance;
    }

    @Override
    public Flowable<List<classtipQuestion>> getQuestions() {
        return iclasstipQuestionDataSource.getQuestions();
    }

    @Override
    public Flowable<List<classtipQuestion>> getQuestionsById(int topicId) {
        return iclasstipQuestionDataSource.getQuestionsById(topicId);
    }

    @Override
    public Flowable<List<classtipQuestion>> getQuestionsById(int topicId, int orderNo) {
        return iclasstipQuestionDataSource.getQuestionsById(topicId, orderNo);
    }

    @Override
    public int isExist(int questionId) {
        return iclasstipQuestionDataSource.isExist(questionId);
    }

    @Override
    public int countQuestions(int topicId) {
        return iclasstipQuestionDataSource.countQuestions(topicId);
    }

    @Override
    public int countQuestions() {
        return iclasstipQuestionDataSource.countQuestions();
    }

    @Override
    public void insertQuestions(classtipQuestion... classtipQuestions) {
        iclasstipQuestionDataSource.insertQuestions(classtipQuestions);
    }

    @Override
    public void updateQuestions(classtipQuestion... classtipQuestions) {
        iclasstipQuestionDataSource.updateQuestions(classtipQuestions);
    }
}
