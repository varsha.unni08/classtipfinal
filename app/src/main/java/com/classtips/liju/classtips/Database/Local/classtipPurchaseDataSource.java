package com.classtips.liju.classtips.Database.Local;

import com.classtips.liju.classtips.Database.DataSource.IclasstipPurchaseDataSource;
import com.classtips.liju.classtips.Database.ModelDB.classtipPurchase;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/15/2018.
 */

public class classtipPurchaseDataSource implements IclasstipPurchaseDataSource {

    private classtipPurchaseDAO purchaseDAO;
    private static classtipPurchaseDataSource instance;

    public classtipPurchaseDataSource(classtipPurchaseDAO purchaseDAO) {
        this.purchaseDAO = purchaseDAO;
    }

    public static classtipPurchaseDataSource getInstance(classtipPurchaseDAO purchaseDAO){
        if (instance==null)
            instance=new classtipPurchaseDataSource(purchaseDAO);
        return instance;
    }

    @Override
    public Flowable<List<classtipPurchase>> getPurchaseList() {
        return purchaseDAO.getPurchaseList();
    }

    @Override
    public int isExist(int subId) {
        return purchaseDAO.isExist(subId);
    }

    @Override
    public int sumPrice() {
        return purchaseDAO.sumPrice();
    }

    @Override
    public int countPurchases() {
        return purchaseDAO.countPurchases();
    }

    @Override
    public void emptyPurchases() {
        purchaseDAO.emptyPurchases();
    }

    @Override
    public void insertPurchases(classtipPurchase... classtipPurchases) {
        purchaseDAO.insertPurchases(classtipPurchases);
    }

    @Override
    public void deletePurchases(classtipPurchase... classtipPurchases) {
        purchaseDAO.deletePurchases(classtipPurchases);
    }
}
