package com.classtips.liju.classtips.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.R;

/**
 * Created by User on 8/9/2018.
 */

public class SubjectViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public ImageView image_product;
    public TextView txt_subjct_name;

    IItemClickListner itemClickListner;

    public void setItemClickListner(IItemClickListner itemClickListner) {
        this.itemClickListner = itemClickListner;
    }

    public SubjectViewHolder(View itemView) {
        super(itemView);

        image_product=(ImageView)itemView.findViewById(R.id.image_product);
        txt_subjct_name=(TextView)itemView.findViewById(R.id.txt_subjct_name);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        itemClickListner.onClick(v);
    }
}
