package com.classtips.liju.classtips.Database.ModelDB;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.classtips.liju.classtips.Model.Question;

import java.util.List;

/**
 * Created by User on 9/8/2018.
 */
@Entity(tableName = "classtipTopic")
public class classtipTopic {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name="topic_id")
    public String topic_id;

    @ColumnInfo(name="topic_name")
    public String topic_name;

    @ColumnInfo(name="subId")
    public String subId;

    @ColumnInfo(name="chptr_id")
    public String chptr_id;

    @ColumnInfo(name="chapName")
    public String chapName;

    @ColumnInfo(name="classmode_id")
    public String classmode_id;

    @ColumnInfo(name="subName")
    public String subName;

    @ColumnInfo(name="priority")
    public String priority;

    @ColumnInfo(name="topic_type")
    public String topic_type;

    @ColumnInfo(name="viewFlag")
    public String viewFlag;

    @ColumnInfo(name="updtn_flag")
    public String updtn_flag;

    @ColumnInfo(name="favId")
    public String favId;

    @ColumnInfo(name="video_id")
    public String video_id="";

    @ColumnInfo(name="url")
    public String url="";

    @ColumnInfo(name="video_name")
    public String video_name="";


    @ColumnInfo(name="youtube_id")
    public String youtube_id="";

    @ColumnInfo(name="video_type")
    public String video_type="";

    @ColumnInfo(name = "questioncount")
    public String questioncount;







   public boolean isdownloading=false;

   public int downloadID=-1;

   public boolean ispaused=false;

   public boolean isresumed=false;

}
