package com.classtips.liju.classtips.Database.ModelDB;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by User on 9/28/2018.
 */
@Entity(tableName = "classtipExam")
public class classtipExam {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name="id")
    public String id;

    @ColumnInfo(name="examName")
    public String examName;

    @ColumnInfo(name="courseId")
    public String courseId;

    @ColumnInfo(name="subId")
    public String subId;

}

