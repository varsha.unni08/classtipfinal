package com.classtips.liju.classtips.Database.DataSource;

import com.classtips.liju.classtips.Database.ModelDB.classtipClassMode;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/24/2018.
 */

public interface IclasstipClassModeDataSource {

    Flowable<List<classtipClassMode>> getClassModes();

    int isExist(int classModeId);

    int countClassModes();

    void  emptyClassModes();

    void insertClassModes(classtipClassMode...classtipClassModes);

    void updateClassmodes(classtipClassMode...classtipClassModes);

}
