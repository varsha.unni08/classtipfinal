package com.classtips.liju.classtips.Database.Local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.classtips.liju.classtips.Database.ModelDB.classtipDownload;

/**
 * Created by User on 10/26/2018.
 */
@Dao
public interface classtipDownloadDAO {

    @Query("SELECT EXISTS(SELECT 1 FROM classtipDownload where id=:downloadId)")
    int isExist(int downloadId);

    @Query("SELECT EXISTS(SELECT 1 FROM classtipDownload where downloadFlag=:flag and id = :downloadId)")
    int isFlgExist(String flag, int downloadId);

    @Query("SELECT COUNT(*) FROM classtipDownload")
    int countSubjects();

    @Query("UPDATE classtipDownload SET downloadFlag=:flag WHERE id = :downloadId")
    void updateDownloads(String flag, int downloadId);

    @Query("Delete  from classtipDownload")
    void  emptyDownloads();

    @Insert
    void insertDownloads(classtipDownload...classtipDownloads);

    @Update
    void updateDownloads(classtipDownload...classtipDownloads);

    @Delete
    void deleteDownloads(classtipDownload...classtipDownloads);
}
