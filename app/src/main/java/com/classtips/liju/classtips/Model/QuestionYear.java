package com.classtips.liju.classtips.Model;

/**
 * Created by User on 9/13/2018.
 */

public class QuestionYear {
    public int question_id;
    public String question_img;
    public String question_video;
    public String question_type;
    public String group_no;
    public String order_no;
    public String question_flag;
    public String updtn_flg;
    public int exam_id;
    public int topic_id;
    public String answer_img;
}
