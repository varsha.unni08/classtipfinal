package com.classtips.liju.classtips.Database.Local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.classtips.liju.classtips.Database.ModelDB.classtipChapter;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/7/2018.
 */
@Dao
public interface classtipChapterDAO {


    @Query("SELECT * FROM classtipChapter")
    Flowable<List<classtipChapter>> getChapters();

    @Query("SELECT * FROM classtipChapter where subId=:subId")
    Flowable<List<classtipChapter>> getChaptersById(int subId);

    @Query("SELECT EXISTS(SELECT 1 FROM classtipChapter where id=:chapId)")
    int isExist(int chapId);

    @Query("SELECT COUNT(*) FROM classtipChapter")
    int countChapters();

    @Query("Delete  from classtipChapter")
    void  emptyChapters();


    @Insert
    void insertChapters(classtipChapter...classtipChapters);

    @Update
    void updateChapters(classtipChapter...classtipChapters);


}
