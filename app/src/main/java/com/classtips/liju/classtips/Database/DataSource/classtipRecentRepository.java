package com.classtips.liju.classtips.Database.DataSource;

import com.classtips.liju.classtips.Database.ModelDB.classtipRecent;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/5/2018.
 */

public class classtipRecentRepository implements IclasstipRecentDataSource {

    private IclasstipRecentDataSource iclasstipRecentDataSource;

    public classtipRecentRepository(IclasstipRecentDataSource iclasstipRecentDataSource) {
        this.iclasstipRecentDataSource = iclasstipRecentDataSource;
    }

    private static classtipRecentRepository instance;

    public static classtipRecentRepository getInstance(IclasstipRecentDataSource iclasstipRecentDataSource){
        if (instance==null)
            instance=new classtipRecentRepository(iclasstipRecentDataSource);
        return instance;
    }

    @Override
    public Flowable<List<classtipRecent>> getRecentVideos() {
        return iclasstipRecentDataSource.getRecentVideos();
    }

    @Override
    public List<classtipRecent> getRecentVideosById(int topicId) {
        return iclasstipRecentDataSource.getRecentVideosById(topicId);
    }

    @Override
    public int isExist(int videoId) {
        return iclasstipRecentDataSource.isExist(videoId);
    }

    @Override
    public int countVideos() {
        return iclasstipRecentDataSource.countVideos();
    }

    @Override
    public void emptyVideos() {
        iclasstipRecentDataSource.emptyVideos();
    }

    @Override
    public void insertVideos(classtipRecent... classtipRecents) {
        iclasstipRecentDataSource.insertVideos(classtipRecents);
    }

    @Override
    public void updateVideos(classtipRecent... classtipRecents) {
        iclasstipRecentDataSource.updateVideos(classtipRecents);
    }

    @Override
    public void deleteVideos(classtipRecent... classtipRecents) {
        iclasstipRecentDataSource.deleteVideos(classtipRecents);
    }
}
