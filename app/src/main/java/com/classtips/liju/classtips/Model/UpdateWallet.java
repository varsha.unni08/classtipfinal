package com.classtips.liju.classtips.Model;

/**
 * Created by User on 9/16/2018.
 */

public class UpdateWallet {
    private boolean updated;
    private String error_msg;

    public UpdateWallet() {
    }

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }
}
