package com.classtips.liju.classtips.Adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.R;

/**
 * Created by User on 8/16/2018.
 */

public class TopicViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView text_topic_name,questiom_count;
    public ImageView imageView,image_favorite,image_download,image_pause;
    public CardView cardView;

   public ProgressBar progressBar2;

    IItemClickListner itemClickListner;

   public LinearLayout layout_downloaded;

    public void setItemClickListner(IItemClickListner itemClickListner) {
        this.itemClickListner = itemClickListner;
    }

    public TopicViewHolder(View itemView) {
        super(itemView);

        text_topic_name=(TextView)itemView.findViewById(R.id.txt_topic_name);
        questiom_count=(TextView)itemView.findViewById(R.id.question_count);
        imageView=(ImageView) itemView.findViewById(R.id.image_product);
        image_favorite=(ImageView) itemView.findViewById(R.id.image_favrt);
        cardView=(CardView)itemView.findViewById(R.id.card_view_topic) ;
        image_download=itemView.findViewById(R.id.image_download);
        progressBar2=itemView.findViewById(R.id.progressBar2);
        layout_downloaded=itemView.findViewById(R.id.layout_downloaded);
        image_pause=itemView.findViewById(R.id.image_pause);
        itemView.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        itemClickListner.onClick(v);
    }
}
