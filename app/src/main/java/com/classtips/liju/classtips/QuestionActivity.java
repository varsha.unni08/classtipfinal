package com.classtips.liju.classtips;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.print.PrintHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.classtips.liju.classtips.Adapter.QuestionAdapter;
import com.classtips.liju.classtips.Database.AdapterDB.QuestionAdapterDB;
import com.classtips.liju.classtips.Database.ModelDB.classtipQuestion;
import com.classtips.liju.classtips.Model.Question;
import com.classtips.liju.classtips.Retrofit.IClassTipAPI;
import com.classtips.liju.classtips.Utils.Common;
import com.classtips.liju.classtips.Utils.DownloadUtils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Notification;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import static com.classtips.liju.classtips.Utils.Common.questionRepository;

public class QuestionActivity extends AppCompatActivity {
    IClassTipAPI mService;
    RecyclerView question_menu;
    TextView txt_message;
    ProgressBar progressBar = null;
    //RxJava
    CompositeDisposable compositeDisposable=new CompositeDisposable();
    QuestionAdapterDB questionAdapterDB;

    String videoData="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);




        mService= Common.getAPI();
        progressBar = findViewById(R.id.progressbar);
        txt_message = findViewById(R.id.txt_msg);
        question_menu = findViewById(R.id.question_menu);
        question_menu.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        question_menu.setHasFixedSize(true);

        questionAdapterDB=new QuestionAdapterDB(this,new ArrayList<classtipQuestion>());
        question_menu.setAdapter(questionAdapterDB);

        if (isConnectingToInternet()) {
            loadQuestionList(Common.currentClasstipTopic.topic_id);
            progressBar.setVisibility(View.VISIBLE);
        }
        else
            loadQuestionListDB(Common.currentClasstipTopic.topic_id);
    }

    private void loadQuestionListDB(String id) {
        compositeDisposable.add(Common.questionRepository.getQuestionsById(Integer.parseInt(id))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<classtipQuestion>>() {
                               @Override
                               public void accept(List<classtipQuestion> classtipQuestions) throws Exception {

                                   List<classtipQuestion> classtip_new=new ArrayList<>();

                                   for (classtipQuestion question:classtipQuestions) {
                                       String fileName = question.question_img.substring(question.question_img.lastIndexOf('/') + 1);

                                       String fileName1 = question.answer_img.substring(question.answer_img.lastIndexOf('/') + 1);


                                       if (iscontentsexist(0, fileName, question.question_id + "")) {

                                           question.isQuestionimgdownloaded = true;
                                       }

                                       if (iscontentsexist(0, fileName1, question.question_id + "")) {

                                           question.isAnswerimgdownloaded = true;
                                       }

                                       if (iscontentsexist(0, question.question_name, question.question_id + "")) {

                                           question.isVideoDownloaded = true;
                                       }


                                       classtip_new.add(question);
                                   }


                                   questionAdapterDB.setQuestionList(classtip_new);
                                   try {
                                       JSONArray req = new JSONArray(new Gson().toJson(classtipQuestions));
                                       if (req.length() == 0) {
                                           txt_message.setVisibility(View.VISIBLE);
                                           final Handler handler = new Handler();
                                           handler.postDelayed(new Runnable() {
                                               @Override
                                               public void run() {
                                                   finish();
                                               }
                                           }, 1000);

                                       }
                                   } catch (JSONException e) {
                                       e.printStackTrace();
                                   }
                               }
                           }
                ));
    }

    private void loadQuestionList(String topic_id) {
        compositeDisposable.add(mService.getQuestion(topic_id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onExceptionResumeNext(new Observable<List<classtipQuestion>>() {
                    @Override
                    protected void subscribeActual(Observer<? super List<classtipQuestion>> observer) {
                    }
                })
                .doOnError(new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
                        Toast.makeText(QuestionActivity.this, "Something went error", Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.GONE);

                    }
                })
                .doOnComplete(new Action() {
                    @Override
                    public void run() throws Exception {

                        progressBar.setVisibility(View.GONE);
                    }
                })
                .doOnEach(new Consumer<Notification<List<classtipQuestion>>>() {
                    @Override
                    public void accept(Notification<List<classtipQuestion>> listNotification) throws Exception {
                        progressBar.setVisibility(View.GONE);
                    }
                })
                .subscribe(new Consumer<List<classtipQuestion>>() {
                    @Override
                    public void accept(List<classtipQuestion> questions) throws Exception {

                        try {
                            JSONArray req = new JSONArray(new Gson().toJson(questions));
                            if (req.length()==0)
                            {
                                txt_message.setVisibility(View.VISIBLE);
                                final Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        finish();
                                    }
                                },1000);

                            }
                            Log.d("classtip234", new Gson().toJson(questions));
                            for (int i = 0; i < req.length(); ++i) {
                                JSONObject rec = req.getJSONObject(i);
                                int id = rec.getInt("question_id");
                                //int examId = rec.getInt("exam_id");
                                int topicId = rec.getInt("topic_id");
                                String question = rec.getString("question_img");
                                String questionType = rec.getString("question_type");
                                String questionVideo = rec.getString("question_video");
                                String orderNo = rec.getString("order_no");
                                String updationFlag = rec.getString("updtn_flg");
                                String answerImg=rec.getString("answer_img");
                                String questionName=rec.getString("question_name");
                                String vimeoUrl=rec.getString("vimeo_url");
                                String questionText=rec.getString("question_text");
                                String examName=rec.getString("exam_name");
//                                String groupNo=rec.getString("group_no");


                                    classtipQuestion classtipQuestion =new classtipQuestion();
                                    classtipQuestion.question_id= String.valueOf(id);
                                    classtipQuestion.question_video=questionVideo;
                                    classtipQuestion.exam_id= String.valueOf(0);
                                    classtipQuestion.topic_id= String.valueOf(topicId);
                                    classtipQuestion.question_img=question;
                                    classtipQuestion.question_type=questionType;
                                    classtipQuestion.order_no= orderNo;
                                    classtipQuestion.answer_img=answerImg;
                                    classtipQuestion.question_name=questionName;
                                    classtipQuestion.vimeo_url=vimeoUrl;
                                    classtipQuestion.question_text=questionText;
                                    classtipQuestion.exam_name = examName;
//                                    classtipQuestion.groupNo=groupNo;


                                String fileName = classtipQuestion.question_img.substring(classtipQuestion.question_img.lastIndexOf('/') + 1);

                                String fileName1 = classtipQuestion.answer_img.substring(classtipQuestion.answer_img.lastIndexOf('/') + 1);


                                if(iscontentsexist(0,fileName,id+""))
                                {

                                    classtipQuestion.isQuestionimgdownloaded=true;
                                }

                                if(iscontentsexist(0,fileName1,id+""))
                                {

                                    classtipQuestion.isAnswerimgdownloaded=true;
                                }

                                if(iscontentsexist(0,classtipQuestion.question_name,id+""))
                                {

                                    classtipQuestion.isVideoDownloaded=true;
                                }


                                if (Common.questionRepository.isExist(id)!=1)
                                {
                                    Common.questionRepository.insertQuestions(classtipQuestion);
                                    Toast.makeText(QuestionActivity.this, new Gson().toJson(Common.questionRepository.countQuestions()), Toast.LENGTH_SHORT).show();
                                    Log.d("classtip23", new Gson().toJson(classtipQuestion));
                                }
                                else
                                {
                                    //questionRepository.updateQuestions(classtipQuestion);
                                    Common.questionRepository.updateQuestions(classtipQuestion);

                                }

                            }

                           // questionAdapterDB.setQuestionList(questions);

                            loadQuestionListDB(Common.currentClasstipTopic.topic_id);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }));
    }

    private void displayQuestionList(List<Question> questions) {
        QuestionAdapter adapter=new QuestionAdapter(this,questions);
        question_menu.setAdapter(adapter);
    }

//    @Override
//    public void onBackPressed() {
//       // super.onBackPressed();
//
//    }

    private boolean isConnectingToInternet() {
    ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(ChapterActivity.CONNECTIVITY_SERVICE);
    NetworkInfo networkInfo = connectivityManager
            .getActiveNetworkInfo();
    if (networkInfo != null && networkInfo.isConnected())
        return true;
    else
        return false;
}

    @Override
    public void onBackPressed() {

//        startActivity(new Intent(QuestionActivity.this, TopicActivity.class));
        finish();
    }

    public boolean iscontentsexist(int position,String filename,String q_id)
    {

        boolean a=false;
        File file_dirq = new File(QuestionActivity.this.getExternalCacheDir() + "/" + Common.currentClasstipSubject.subject_id + "/" + Common.currentClasstipTopic.chptr_id + "/" + Common.currentClasstipTopic.topic_id + "/"+q_id);

        if(file_dirq.exists())
        {
            if(DownloadUtils.getFilelist(file_dirq).size()>0)
            {

                for (File file :DownloadUtils.getFilelist(file_dirq))
                {

                    if(file.getName().contains(filename))
                    {
                        a=true;
                        break;

                    }


                }


            }

        }



        return  a;
    }
}
