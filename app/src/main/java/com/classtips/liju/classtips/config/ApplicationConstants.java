package com.classtips.liju.classtips.config;

public interface ApplicationConstants {
    int TIMEOUT_VALUE = 60000;
    int FINISH_KEY = 12;
    int STAY_ALERT_KEY = 11;
    int COMP_ID = 1;
    int APP_ID = 1;
    int API_SUCCESS = 6000;
    int HTTP_CALL_SUCCESS = 200;
    int API_FAILURE = 6001;
    //Widgets Identifier
//    int STOCK_VALUE = 1;
    int TANK_WISE_INVENTORY = 2;
    int CASH_AND_BANK_BALANCE = 3;
    int KEY_OPERATIONAL_INDICATORS = 4;
    int FUEL_STOCK_POSITION = 5;
    int WORKING_CAPITAL_INVESTMENT = 6;
    int SALES_TRENDS_FUEL_PRODUCTS = 7;
    int TOP_FIVE_OUTSTANDING = 8;
    int AVERAGE_STOCK_HOLDING_PERIOD = 9;
    int TODAYS_SNAPSHOT = 10;
    int SALES_TRENDS_OTHER_PRODUCTS = 11;
    int P_AND_L_TRENDS = 12;
    int BALANCE_SHEET = 13;
    int PRICE_CHANGE_IMPACT = 14;
    //
    int REPORTS_OPERATIONS = 1;
    int REPORTS_FINANCE = 2;
    int REPORTS_OTHERS = 3;
    int CALL_PHONE_REQUEST_CODE = 201;
    int KEY_HOME_PAGE = 1;
    int KEY_MY_PUMP_PAGE = 0;

    int MENU_PAGE_ID_MY_PUMP = 10000;
    int MENU_PAGE_ID_REPORTS = 10100;
    int MENU_PAGE_ID_CONTROL_PANEL = 10200;
    int MENU_PAGE_ID_SUBSCRIPTIONS = 10300;
    int MENU_PAGE_ID_ABOUT_US = 10500;

    int PIN_TYPE_CREATE = 2;
    int PIN_TYPE_CONFIRM = 1;

    String PHONE = "phone";
    String COURSE_ID = "course";
    String USER_NAME = "name";
    String INSTALLATION_DATE ="date";
    String EXPIRY_DATE ="date";

    String IS_PIN_CREATED = "isPinCreated";
    String IS_TWO_FACTOR_AUTH_ENABLED = "isTwoFactorAuthEnabled";
    String LOGIN_PIN = "loginPin";
    String DEFAULT_LANDING_PAGE = "defaultLandingPage";

    String LOGGED_STATUS = "loggedStatus";
    String MOBILE_NUMBER_STATUS = "numberVerification";
    String LICENCE_STATUS = "licenceStatus";
    String BULK_DATA_STATUS = "dataStatus";
    String CONTROL_PANEL_TUTORIAL_DISMISSED = "controlPanelTutorialDismissed";
    String APPLICATION_DATA = "applicationData";
//    String SELECTED_ROLE = "selectedRole";
    String MY_PUMP = "My Pump";
    String REPORTS = "Reports";
    String CONTROL_PANEL = "Control Panel";
    String CONTROL_PANEL_FORM = "Control Panel1";
    String WIDGET_ENABLE = "Widget Enable";
    String USER_CONTROL = "User Control";
    String ALERTS = "Alerts";
    String ACTIONS = "Actions";
    String _STOCK_VALUE = "Stock Value";
    String _TANK_WISE_INVENTORY = "Tank Wise Inventory";
    String _CASH_AND_BANK_BALANCE = "Cash and Bank Balance";
    String _KEY_OPERATIONAL_INDICATORS = "Key Operational Indicators";
    String _FUEL_STOCK_POSITION = "Fuel Stock Position";
    String _WORKING_CAPITAL_INVESTMENTS_AND_RETURNS = "Working Capital Investment and Returns";
    String _SALES_TRENDS_FUEL_PRODUCTS = "Sales Trends - Fuel Products";
    String _TOP_FIVE_OUTSTANDING = "Top 5 Outstanding";
    String _AVERAGE_STOCK_HOLDING_PERIOD = "Average Stock Holding Period";
    String _TODAYS_SNAPSHOT = "Today’s Snapshot";
    String _SALES_TRENDS_OTHER_PRODUCTS = "Sales Trends - Other Products";
    String _P_AND_L_TRENDS = "P & L Trends";
    String _BALANCE_SHEET = "Balance Sheet";
    String _PRICE_CHANGE_IMPACT = "Price Change Impact";
    String SUPPORT_CONTACT_NUMBER = "99122323";
    String URL = "url";

    String qvideo="qvideo";


}
