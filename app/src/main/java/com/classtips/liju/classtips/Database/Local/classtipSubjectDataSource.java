package com.classtips.liju.classtips.Database.Local;

import com.classtips.liju.classtips.Database.DataSource.IclasstipSubjectDatasource;
import com.classtips.liju.classtips.Database.ModelDB.classtipSubject;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/7/2018.
 */

public class classtipSubjectDataSource implements IclasstipSubjectDatasource {

    private classtipSubjectDAO subjectDAO;
    private static classtipSubjectDataSource instance;

    public classtipSubjectDataSource(classtipSubjectDAO subjectDAO) {
        this.subjectDAO = subjectDAO;
    }

    public static classtipSubjectDataSource getInstance(classtipSubjectDAO subjectDAO){
        if (instance==null)
            instance=new classtipSubjectDataSource(subjectDAO);
                    return instance;
    }

    @Override
    public Flowable<List<classtipSubject>> getSubjects() {
        return subjectDAO.getSubjects();
    }

    @Override
    public Flowable<List<classtipSubject>> getSubjectsById(int courseId) {
        return subjectDAO.getSubjectsById(courseId);
    }

    @Override
    public int isExist(int subId) {
        return subjectDAO.isExist(subId);
    }

    @Override
    public int countSubjects() {
        return subjectDAO.countSubjects();
    }

    @Override
    public void emptySubjects() {
        subjectDAO.emptySubjects();
    }

    @Override
    public void insertSubjects(classtipSubject... classtipSubjects) {
        subjectDAO.insertSubjects(classtipSubjects);
    }

    @Override
    public void updateSubjects(classtipSubject... classtipSubjects) {
        subjectDAO.updateSubjects(classtipSubjects);
    }

    @Override
    public void deleteSubjects(classtipSubject... classtipSubjects) {
        subjectDAO.deleteSubjects(classtipSubjects);
    }
}
