package com.classtips.liju.classtips.Database.Local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.classtips.liju.classtips.Database.ModelDB.classtipUser;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/12/2018.
 */
@Dao
public interface classtipUserDAO {

    @Query("SELECT * FROM classtipUser")
    Flowable<List<classtipUser>> getUsers();

    @Query("SELECT * FROM classtipUser where courseId=:courseId")
    Flowable<List<classtipUser>> getUsersById(int courseId);

    @Query("SELECT EXISTS(SELECT 1 FROM classtipUser where phone=:phone)")
    int isExist(int phone);

    @Query("SELECT EXISTS(SELECT 1 FROM classtipUser where updtnFlag=:flag and phone=:phone)")
    int isUpdated(String flag,String phone);

    @Query("UPDATE classtipUser SET updtnFlag=:flag WHERE phone=:phone")
    void updateUser(String flag, String phone);

    @Query("SELECT COUNT(*) FROM classtipUser")
    int countUsers();

    @Insert
    void insertUsers(classtipUser...classtipUsers);

    @Query("UPDATE classtipUser SET instlnDate=:date WHERE phone=:phone")
    void updateUserDate(String date, String phone);

    @Query("UPDATE classtipUser SET expiryFlag=:flag WHERE phone=:phone")
    void updateExpiryFlag(String flag, String phone);

}
