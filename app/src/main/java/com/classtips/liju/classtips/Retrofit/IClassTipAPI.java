package com.classtips.liju.classtips.Retrofit;

import com.classtips.liju.classtips.Database.ModelDB.classtipChapter;
import com.classtips.liju.classtips.Database.ModelDB.classtipClassMode;
import com.classtips.liju.classtips.Database.ModelDB.classtipQuestion;
import com.classtips.liju.classtips.Database.ModelDB.classtipSubject;
import com.classtips.liju.classtips.Database.ModelDB.classtipTopic;
import com.classtips.liju.classtips.Model.ChapterTopic;
import com.classtips.liju.classtips.Model.CheckPurchaseResponse;
import com.classtips.liju.classtips.Model.CheckUserResponse;
import com.classtips.liju.classtips.Model.ClassMode;
import com.classtips.liju.classtips.Model.Course;
import com.classtips.liju.classtips.Model.DownloadList;
import com.classtips.liju.classtips.Model.Exams;
import com.classtips.liju.classtips.Model.MainVideo;
import com.classtips.liju.classtips.Model.MoreDetailVideo;
import com.classtips.liju.classtips.Model.Purchase;
import com.classtips.liju.classtips.Model.QRcode;
import com.classtips.liju.classtips.Model.Question;
import com.classtips.liju.classtips.Model.QuestionVideo;
import com.classtips.liju.classtips.Model.QuestionYear;
import com.classtips.liju.classtips.Model.Subject;
import com.classtips.liju.classtips.Model.Topic;
import com.classtips.liju.classtips.Model.UpdateFlag;
import com.classtips.liju.classtips.Model.UpdateWallet;
import com.classtips.liju.classtips.Model.User;
import com.classtips.liju.classtips.Model.UserComment;
import com.classtips.liju.classtips.Model.Video;
import com.classtips.liju.classtips.Model.Wallet;
import com.google.gson.JsonArray;

import org.json.JSONArray;

import java.util.List;
import io.reactivex.Observable;
//import java.util.Observable;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by LIJU on 8/4/2018.
 */

public interface IClassTipAPI {

    @FormUrlEncoded
    @POST("getqrcode.php")
    Call<QRcode>getQRcode(@Field("qrcode") String qrCode,
                          @Field("serialno") String serialNo,
                          @Field("phone") String phone);

    @FormUrlEncoded
    @POST("checkuser.php")
    Call<CheckUserResponse>checkUserExists(@Field("phone") String phone);

    @FormUrlEncoded
    @POST("updatewallet.php")
    Call<UpdateWallet>updateWalet(@Field("phone") String phone,
                                  @Field("money") String money);

    @FormUrlEncoded
    @POST("updatepurchaseflag.php")
    Call<UpdateFlag>updatePurchaseFlag(@Field("phone") String phone,
                               @Field("course") String course);

    @FormUrlEncoded
    @POST("updateflag.php")
    Call<UpdateFlag>updateFlag(@Field("phone") String phone,
                               @Field("flag") String flag);

    @FormUrlEncoded
    @POST("updatedate.php")
    Call<UpdateFlag>updateDate(@Field("phone") String phone);

    @FormUrlEncoded
    @POST("updateexpiryflag.php")
    Call<UpdateFlag>updateExpiryFlag(@Field("phone") String phone);

    @FormUrlEncoded
    @POST("updatetopicupdation.php")
    Call<UpdateFlag>updateTopicFlag(@Field("phone") String phone,
                               @Field("flag") String flag);

    @FormUrlEncoded
    @POST("updatevideoflag.php")
    Call<UpdateFlag>updateVideoFlag(@Field("phone") String phone,
                                    @Field("flag") String flag);

    @FormUrlEncoded
    @POST("updatequestionflag.php")
    Call<UpdateFlag>updateQuestionFlag(@Field("phone") String phone,
                                    @Field("flag") String flag);

    @FormUrlEncoded
    @POST("checkpurchase.php")
    Call<CheckPurchaseResponse>checkPurchaseExists(@Field("phone") String phone,
                                                   @Field("subid") String subID);

    @FormUrlEncoded
    @POST("coppyuser.php")
    Call<UpdateFlag>coppyUser(@Field("phone") String phone);

    @FormUrlEncoded
    @POST("register.php")
    Call<User>registerNewUser(@Field("phone") String phone,
                              @Field("name") String name,
                              @Field("address") String address,
                              @Field("birthdate") String birthdate,
                              @Field("course") String course,
                              @Field("expirydate") String expiryDate,
                              @Field("imei") String imei,
                              @Field("version") String version,
                              @Field("flag") String flag);

    @FormUrlEncoded
    @POST("insertcomment.php")
    Call<UserComment>insertNewComment(@Field("phone") String phone,
                                      @Field("name") String name,
                                      @Field("topicid") String topicId,
                                      @Field("videoid") String videoId,
                                      @Field("comment") String comment);

    @FormUrlEncoded
    @POST("purchase.php")
    Call<Purchase>purchaseNewOrder(@Field("courseid") String courseID,
                                  @Field("subid") String subID,
                                  @Field("phone") String phone,
                                  @Field("comment") String comment);

    @FormUrlEncoded
    @POST("getuser.php")
    Call<User>getUserInformation(@Field("phone") String phone);

    @FormUrlEncoded
    @POST("getwalletinformation.php")
    Call<Wallet>getWalletInformation(@Field("phone") String phone);

    @FormUrlEncoded
    @POST("getvideo.php")
    Call<Video>getVideos(@Field("topic") String TopicID);

    @FormUrlEncoded
    @POST("getnexttopicvideo.php")
    Call<Video>getNextVideos(@Field("chapid") String chapID,
                             @Field("priority") String priority);

    @FormUrlEncoded
    @POST("getcrashvideo.php")
    Call<Video>getCrashVideos(@Field("chapid") String chapId,
                              @Field("classmode") String classmode);

    @FormUrlEncoded
    @POST("getquestionvideo.php")
    Call<QuestionVideo>getQuestionVideos(@Field("topicid") String TopicID,
                                         @Field("orderno") String orderNo);

    @FormUrlEncoded
    @POST("submitcart.php")
    Call<String>newOrder(@Field("order") String order);

    @FormUrlEncoded
    @POST("getmoredetailvideo.php")
    Observable<List<Video>> getMoreVideos(@Field("topicid") String topicID);

    @FormUrlEncoded
    @POST("getvideobycourse.php")
    Observable<List<MainVideo>> getMainVideos(@Field("courseid") String courseID);

    @FormUrlEncoded
    @POST("getquestion.php")
    Observable<List<Question>> getAllQuestions(@Field("courseid") String courseID);


    @FormUrlEncoded
    @POST("getquestiontest.php")
    Observable<List<classtipQuestion>> getQuestion(@Field("topicid") String topicID);

    @FormUrlEncoded
    @POST("getquestionyeartest.php")
    Observable<List<QuestionYear>> getQuestionYear(@Field("examid") String examID);

    @FormUrlEncoded
    @POST("getquestionyearbycourse.php")
    Observable<List<QuestionYear>> getAllYearWiseQuestion(@Field("courseid") String courseID);


    @FormUrlEncoded
    @POST("getchaptertopic.php")
    Observable<List<classtipChapter>> getChapter(@Field("subid") String subID);

    @FormUrlEncoded
    @POST("getchapter.php")
    Observable<List<ChapterTopic>> getChapterByCourse(@Field("courseid") String courseID);

    @FormUrlEncoded
    @POST("gettopic.php")
    Observable<List<Topic>> getTopic(@Field("courseid") String courseID);

    @FormUrlEncoded
    @POST("gettopicbyid.php")
    Call<JsonArray> getTopicById(@Field("chapid") String chapID,
                                 @Field("classmodeid") String classmodeId);

    @FormUrlEncoded
    @POST("getsubject.php")
    Observable<List<classtipSubject>> getSubjects(@Field("courseid") String courseID);


    @FormUrlEncoded
    @POST("getsubject.php")
    Observable<List<classtipSubject>> getSubjectswithpurchase(@Field("courseid") String courseID,@Field("phone")String phone);

    @FormUrlEncoded
    @POST("getdownloadlist.php")
    Observable<List<DownloadList>> getDownloadList(@Field("courseid") String courseID);

    @FormUrlEncoded
    @POST("getsubjectpurchase.php")
    Observable<List<classtipSubject>> getSubjectPurchase(@Field("courseid") String courseID,
                                                 @Field("phone") String phone);

    @FormUrlEncoded
    @POST("getexams.php")
    Observable<List<Exams>> getExams(@Field("subid") String subID,
                                     @Field("courseid") String courseID);

    @FormUrlEncoded
    @POST("getexamsbyid.php")
    Observable<List<Exams>> getExamsByCourseId(@Field("courseid") String courseID);

    @FormUrlEncoded
    @POST("getwalletmoney.php")
    Observable<List<Wallet>> getWallet(@Field("phone") String phone);

    @GET("getcourse.php")
    Observable<List<Course>> getCourses();

    @GET("getclassmode.php")
    Observable<List<classtipClassMode>> getClassmodes();

}
