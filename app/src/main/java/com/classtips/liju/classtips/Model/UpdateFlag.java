package com.classtips.liju.classtips.Model;

/**
 * Created by User on 11/3/2018.
 */

public class UpdateFlag {
    private boolean updated;
    private String error_msg;
    private boolean flagupdated;
    private boolean expiryflagupdated;
    private boolean videoflagupdated;
    private boolean questionflagupdated;
    private boolean dateupdated;
    private boolean purchaseflagupdated;
    private String flagerror_msg;
    private boolean coppied;
    private String expiryflagerror_msg;
    private String dateerror_msg;
    private String videoflagerror_msg;
    private String questionflagerror_msg;
    private String coppyerror_msg;
    private String purchaseflagerror_msg;

    public UpdateFlag() {
    }

    public boolean isFlagupdated() {
        return flagupdated;
    }

    public void setFlagupdated(boolean flagupdated) {
        this.flagupdated = flagupdated;
    }

    public String getFlagerror_msg() {
        return flagerror_msg;
    }

    public void setFlagerror_msg(String flagerror_msg) {
        this.flagerror_msg = flagerror_msg;
    }

    public boolean isExpiryflagupdated() {
        return expiryflagupdated;
    }

    public void setExpiryflagupdated(boolean expiryflagupdated) {
        this.expiryflagupdated = expiryflagupdated;
    }

    public String getExpiryflagerror_msg() {
        return expiryflagerror_msg;
    }

    public void setExpiryflagerror_msg(String expiryflagerror_msg) {
        this.expiryflagerror_msg = expiryflagerror_msg;
    }

    public boolean isVideoflagupdated() {
        return videoflagupdated;
    }

    public void setVideoflagupdated(boolean videoflagupdated) {
        this.videoflagupdated = videoflagupdated;
    }

    public String getVideoflagerror_msg() {
        return videoflagerror_msg;
    }

    public void setVideoflagerror_msg(String videoflagerror_msg) {
        this.videoflagerror_msg = videoflagerror_msg;
    }

    public boolean isUpdated() {
        return updated;
    }

    public void setUpdated(boolean updated) {
        this.updated = updated;
    }

    public boolean isQuestionflagupdated() {
        return questionflagupdated;
    }

    public void setQuestionflagupdated(boolean questionflagupdated) {
        this.questionflagupdated = questionflagupdated;
    }

    public String getQuestionflagerror_msg() {
        return questionflagerror_msg;
    }

    public void setQuestionflagerror_msg(String questionflagerror_msg) {
        this.questionflagerror_msg = questionflagerror_msg;
    }

    public boolean isDateupdated() {
        return dateupdated;
    }

    public void setDateupdated(boolean dateupdated) {
        this.dateupdated = dateupdated;
    }

    public boolean isCoppied() {
        return coppied;
    }

    public void setCoppied(boolean coppied) {
        this.coppied = coppied;
    }

    public boolean isPurchaseflagupdated() {
        return purchaseflagupdated;
    }

    public void setPurchaseflagupdated(boolean purchaseflagupdated) {
        this.purchaseflagupdated = purchaseflagupdated;
    }

    public String getPurchaseflagerror_msg() {
        return purchaseflagerror_msg;
    }

    public void setPurchaseflagerror_msg(String purchaseflagerror_msg) {
        this.purchaseflagerror_msg = purchaseflagerror_msg;
    }

    public String getCoppyerror_msg() {
        return coppyerror_msg;
    }

    public void setCoppyerror_msg(String coppyerror_msg) {
        this.coppyerror_msg = coppyerror_msg;
    }

    public String getDateerror_msg() {
        return dateerror_msg;
    }

    public void setDateerror_msg(String dateerror_msg) {
        this.dateerror_msg = dateerror_msg;
    }

    public String getError_msg() {
        return error_msg;
    }

    public void setError_msg(String error_msg) {
        this.error_msg = error_msg;
    }
}
