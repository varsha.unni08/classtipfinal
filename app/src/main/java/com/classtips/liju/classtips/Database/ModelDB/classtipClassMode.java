package com.classtips.liju.classtips.Database.ModelDB;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by User on 9/24/2018.
 */
@Entity(tableName = "classtipClassMode")
public class classtipClassMode {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name="class_mode_id")
    public String class_mode_id;

    @ColumnInfo(name="class_mode_name")
    public String class_mode_name;

    @ColumnInfo(name="link")
    public String link;

    @ColumnInfo(name="flag")
    public String flag;
}
