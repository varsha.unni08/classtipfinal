package com.classtips.liju.classtips.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.classtips.liju.classtips.ChapterActivity;
import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.Model.Subject;
import com.classtips.liju.classtips.QuestionYearActivity;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.Utils.Common;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by User on 8/9/2018.
 */

public class Subject2Adapter extends RecyclerView.Adapter<SubjectViewHolder> {
    Context context;
    List<Subject> subjects;
    String expiryDate,instlnDate;

    public Subject2Adapter(Context context, List<Subject> subjects) {
        this.context = context;
        this.subjects = subjects;
    }

    @NonNull
    @Override
    public SubjectViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(context).inflate(R.layout.subj_menu_layout,null);
        return new SubjectViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SubjectViewHolder holder, final int position) {
        //Load image
        Picasso.with(context)
                .load(subjects.get(position).link)
                .into(holder.image_product);
        holder.txt_subjct_name.setText(subjects.get(position).subject_name);

        //Event
        holder.setItemClickListner(new IItemClickListner() {
            @Override
            public void onClick(View v) {
                Common.currentSubject= subjects.get(position);
                Common.currentClasstipSubject=null;

                SharedPreferences pref = context.getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                SharedPreferences.Editor editor = pref.edit();
                expiryDate=pref.getString("expiryDate", null);
                instlnDate=pref.getString("instlnDate", null);

                SimpleDateFormat dateFormat = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
                Date date1 = null;
                try {
                    date1 = dateFormat.parse(instlnDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Date date2 = null;
                try {
                    date2 = dateFormat.parse(expiryDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                editor.commit();

                if (date1.equals(date2)||date1.after(date2)){
                    Toast.makeText(context, "Your app is expired", Toast.LENGTH_LONG).show();
                }
                //Start new activity
                else{
                    try{
                        if (!Common.currentClassmode.flag.equals("q2"))
                        {
                            context.startActivity(new Intent(context,ChapterActivity.class));
                        }
                        else{
                            //Toast.makeText(context, "New Activity", Toast.LENGTH_SHORT).show();
                            context.startActivity(new Intent(context, QuestionYearActivity.class));
                        }
                    }catch(NullPointerException e){
                        if (!Common.currentClasstipClassMode.flag.equals("q2"))
                        {
                            context.startActivity(new Intent(context,ChapterActivity.class));
                        }
                        else{
                            //Toast.makeText(context, "New Activity", Toast.LENGTH_SHORT).show();
                            context.startActivity(new Intent(context, QuestionYearActivity.class));
                        }
                    }
                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return subjects.size();
    }
}
