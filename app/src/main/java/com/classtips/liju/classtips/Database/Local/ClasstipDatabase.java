package com.classtips.liju.classtips.Database.Local;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.support.annotation.NonNull;

import com.classtips.liju.classtips.Database.ModelDB.classtipChapter;
import com.classtips.liju.classtips.Database.ModelDB.classtipClassMode;
import com.classtips.liju.classtips.Database.ModelDB.classtipDownload;
import com.classtips.liju.classtips.Database.ModelDB.classtipExam;
import com.classtips.liju.classtips.Database.ModelDB.classtipPurchase;
import com.classtips.liju.classtips.Database.ModelDB.classtipQuestion;
import com.classtips.liju.classtips.Database.ModelDB.classtipQuestionYear;
import com.classtips.liju.classtips.Database.ModelDB.classtipRecent;
import com.classtips.liju.classtips.Database.ModelDB.classtipSubject;
import com.classtips.liju.classtips.Database.ModelDB.classtipTopic;
import com.classtips.liju.classtips.Database.ModelDB.classtipUser;

/**
 * Created by User on 9/5/2018.
 */
@Database(entities = {classtipRecent.class,classtipSubject.class,classtipChapter.class, classtipTopic.class, classtipUser.class,
        classtipPurchase.class,classtipClassMode.class,classtipQuestion.class,classtipQuestionYear.class, classtipExam.class, classtipDownload.class},version = 3,exportSchema = true)
public abstract class ClasstipDatabase extends RoomDatabase {

    public abstract classtipRecentDAO recentDAO();
    public abstract classtipSubjectDAO subjectDAO();
    public abstract classtipChapterDAO chapterDAO();
    public abstract classtipTopicDAO topicDAO();
    public abstract classtipUserDAO userDAO();
    public abstract classtipPurchaseDAO purchaseDAO();
    public abstract classtipClassModeDAO classModeDAO();
    public abstract classtipQuestionDAO questionDAO();
    public abstract classtipQuestionYearDAO questionYearDAO();
    public abstract classtipExamDAO examDAO();
    public abstract classtipDownloadDAO downloadDAO();
    public static ClasstipDatabase instance;

    public static ClasstipDatabase getInstance(Context context){
        if (instance==null)

            instance= Room.databaseBuilder(context,ClasstipDatabase.class,"ClasstipDatabase")
                    .allowMainThreadQueries().addMigrations(MIGRATION_1_2)
                    .build();
            return instance;

    }


   public static Migration MIGRATION_1_2 = new Migration(2, 3) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE classtipTopic "
                    +"ADD COLUMN video_id TEXT");

            database.execSQL("ALTER TABLE classtipTopic "
                    +"ADD COLUMN url TEXT");

            database.execSQL("ALTER TABLE classtipTopic "
                    +"ADD COLUMN video_name TEXT");


            database.execSQL("ALTER TABLE classtipTopic "
                    +"ADD COLUMN video_data BLOB");

//            database.execSQL("ALTER TABLE classtipUser "
//                    +"ADD COLUMN id TEXT NOT NULL PRIMARY KEY");




        }
    };
}
