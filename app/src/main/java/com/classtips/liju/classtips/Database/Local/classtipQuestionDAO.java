package com.classtips.liju.classtips.Database.Local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.classtips.liju.classtips.Database.ModelDB.classtipQuestion;

import java.util.List;

import io.reactivex.Flowable;

/**
 * Created by User on 9/27/2018.
 */
@Dao
public interface classtipQuestionDAO {

    @Query("SELECT * FROM classtipQuestion")
    Flowable<List<classtipQuestion>> getQuestions();

    @Query("SELECT * FROM classtipQuestion where topic_id=:topicId order by order_no asc")
    Flowable<List<classtipQuestion>> getQuestionsById(int topicId);

    @Query("SELECT * FROM classtipQuestion where topic_id=:topicId and order_no=:orderNo")
    Flowable<List<classtipQuestion>> getQuestionsById(int topicId,int orderNo);

    @Query("SELECT EXISTS(SELECT 1 FROM classtipQuestion where question_id=:questionId)")
    int isExist(int questionId);

    @Query("SELECT COUNT(*) FROM classtipQuestion where topic_id=:topicId")
    int countQuestions(int topicId);

    @Query("SELECT COUNT(*) FROM classtipQuestion")
    int countQuestions();

    @Insert
    void insertQuestions(classtipQuestion...classtipQuestions);

    @Update
    void updateQuestions(classtipQuestion...classtipQuestions);
}
