package com.classtips.liju.classtips.Database.AdapterDB;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.classtips.liju.classtips.Adapter.QuestionViewHolder;
import com.classtips.liju.classtips.Adapter.QuestionYearViewHolder;
import com.classtips.liju.classtips.Database.ModelDB.classtipQuestionYear;
import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.QuestionVideoActivity;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.Utils.Common;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by User on 9/28/2018.
 */

public class QuestionYearAdapterDB extends RecyclerView.Adapter<QuestionYearViewHolder> {
    Context context;
    List<classtipQuestionYear> classtipQuestions;

    public QuestionYearAdapterDB(Context context, List<classtipQuestionYear> classtipQuestions) {
        this.context = context;
        this.classtipQuestions = classtipQuestions;
    }

    @Override
    public QuestionYearViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(context).inflate(R.layout.question_year_item_layout,null);
        return new QuestionYearViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(QuestionYearViewHolder holder, final int position) {

        Picasso.with(context)
                .load(classtipQuestions.get(position).question)
                .into(holder.image_product);
        holder.text_watch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.currentClasstipQuestionYear=classtipQuestions.get(position);
                Common.currentQuestionYear=null;

                Intent intent=new Intent(context,QuestionVideoActivity.class);
                intent.putExtra("key","1");
                context.startActivity(intent);
            }
        });
        holder.text_answer_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder answerView = new AlertDialog.Builder(context);
                LayoutInflater factory = LayoutInflater.from(v.getRootView().getContext());
                final View view = factory.inflate(R.layout.question_answer_layout, null);
                ImageView img_answer;
                img_answer=(ImageView)view.findViewById(R.id.img_answer);

                try{
                    Picasso.with(context)
                            .load(classtipQuestions.get(position).answerImage)
                            .into(img_answer);
                }catch (Exception e){

                }

                answerView.setView(view);
                answerView.show();
//
            }
        });
        holder.image_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder zoomView = new AlertDialog.Builder(context);
                LayoutInflater factory = LayoutInflater.from(v.getRootView().getContext());
                final View view = factory.inflate(R.layout.zooming_layout, null);
//                ImageView image_product;
                PhotoView photoView;
                photoView=(PhotoView)view.findViewById(R.id.photo_view);
                Picasso.with(context)
                        .load(classtipQuestions.get(position).question)
                        .into(photoView);
                zoomView.setView(view);

                zoomView.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return classtipQuestions.size();
    }
}
