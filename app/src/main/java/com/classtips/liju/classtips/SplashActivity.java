package com.classtips.liju.classtips;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import com.classtips.liju.classtips.Database.DataSource.classtipChapterRepository;
import com.classtips.liju.classtips.Database.DataSource.classtipClassModeRepository;
import com.classtips.liju.classtips.Database.DataSource.classtipDownloadRepository;
import com.classtips.liju.classtips.Database.DataSource.classtipExamRepository;
import com.classtips.liju.classtips.Database.DataSource.classtipPurchaseRepository;
import com.classtips.liju.classtips.Database.DataSource.classtipQuestionRepository;
import com.classtips.liju.classtips.Database.DataSource.classtipQuestionYearRepository;
import com.classtips.liju.classtips.Database.DataSource.classtipRecentRepository;
import com.classtips.liju.classtips.Database.DataSource.classtipSubjectRepository;
import com.classtips.liju.classtips.Database.DataSource.classtipTopicRepository;
import com.classtips.liju.classtips.Database.DataSource.classtipUserRepository;
import com.classtips.liju.classtips.Database.Local.ClasstipDatabase;
import com.classtips.liju.classtips.Database.Local.classtipChapterDataSource;
import com.classtips.liju.classtips.Database.Local.classtipClassModeDataSource;
import com.classtips.liju.classtips.Database.Local.classtipDownloadDataSource;
import com.classtips.liju.classtips.Database.Local.classtipExamDataSource;
import com.classtips.liju.classtips.Database.Local.classtipPurchaseDataSource;
import com.classtips.liju.classtips.Database.Local.classtipQuestionDataSource;
import com.classtips.liju.classtips.Database.Local.classtipQuestionYearDataSource;
import com.classtips.liju.classtips.Database.Local.classtipRecentDataSource;
import com.classtips.liju.classtips.Database.Local.classtipSubjectDataSource;
import com.classtips.liju.classtips.Database.Local.classtipTopicDataSource;
import com.classtips.liju.classtips.Database.Local.classtipUserDataSource;
import com.classtips.liju.classtips.Utils.Common;
import com.classtips.liju.classtips.config.AppPreferences;
import com.classtips.liju.classtips.config.ApplicationConstants;

import net.khirr.android.privacypolicy.PrivacyPolicyDialog;

import io.reactivex.disposables.CompositeDisposable;

public class SplashActivity extends AppCompatActivity implements ApplicationConstants {
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        initDB();
        final AppPreferences appPreferences = AppPreferences.getInstance(SplashActivity.this, APPLICATION_DATA);
        if (appPreferences.getBoolean(LOGGED_STATUS)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                    finish();
                }
            }, 1000);

        }else if (appPreferences.getBoolean(LICENCE_STATUS)){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                }
            }, 1000);
        }else {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    PrivacyPolicyDialog dialog = new PrivacyPolicyDialog(SplashActivity.this,
                            "https://localhost/terms",
                            "https://localhost/privacy");
                    dialog.addPoliceLine("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
                    dialog.addPoliceLine("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
                    dialog.addPoliceLine("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.");
                    dialog.addPoliceLine("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
                    dialog.setAcceptButtonColor(R.color.colorPrimary);
                    dialog.show();

                    dialog.setOnClickListener(new PrivacyPolicyDialog.OnClickListener() {
                        @Override
                        public void onAccept(boolean isFirstTime) {
                            appPreferences.saveBoolean(LICENCE_STATUS, true);
                            startActivity(new Intent(SplashActivity.this, MainActivity.class));
                            finish();
                        }

                        @Override
                        public void onCancel() {
                            finish();
                        }
                    });

                }
            }, 1000);
        }
    }

    private void initDB() {

        Common.classtipDatabase = ClasstipDatabase.getInstance(this);
        Common.recentRepository = classtipRecentRepository.getInstance(classtipRecentDataSource.getInstance(Common.classtipDatabase.recentDAO()));
        Common.subjectRepository = classtipSubjectRepository.getInstance(classtipSubjectDataSource.getInstance(Common.classtipDatabase.subjectDAO()));
        Common.chapterRepository = classtipChapterRepository.getInstance(classtipChapterDataSource.getInstance(Common.classtipDatabase.chapterDAO()));
        Common.topicRepository = classtipTopicRepository.getInstance(classtipTopicDataSource.getInstance(Common.classtipDatabase.topicDAO()));
        Common.userRepository = classtipUserRepository.getInstance(classtipUserDataSource.getInstance(Common.classtipDatabase.userDAO()));
        Common.purchaseRepository = classtipPurchaseRepository.getInstance(classtipPurchaseDataSource.getInstance(Common.classtipDatabase.purchaseDAO()));
        Common.classModeRepository = classtipClassModeRepository.getInstance(classtipClassModeDataSource.getInstance(Common.classtipDatabase.classModeDAO()));
        Common.questionRepository = classtipQuestionRepository.getInstance(classtipQuestionDataSource.getInstance(Common.classtipDatabase.questionDAO()));
        Common.questionYearRepository = classtipQuestionYearRepository.getInstance(classtipQuestionYearDataSource.getInstance(Common.classtipDatabase.questionYearDAO()));
        Common.examRepository = classtipExamRepository.getInstance(classtipExamDataSource.getInstance(Common.classtipDatabase.examDAO()));
        Common.downloadRepository = classtipDownloadRepository.getInstance(classtipDownloadDataSource.getInstance(Common.classtipDatabase.downloadDAO()));

    }

    public Intent getFacebookIntent(String url) {

        PackageManager pm = getPackageManager();
        Uri uri = Uri.parse(url);

        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo("com.facebook.katana", 0);
            if (applicationInfo.enabled) {
                uri = Uri.parse("fb://facewebmodal/f?href=" + url);
            }
        } catch (PackageManager.NameNotFoundException ignored) {
        }

        return new Intent(Intent.ACTION_VIEW, uri);
    }
}
