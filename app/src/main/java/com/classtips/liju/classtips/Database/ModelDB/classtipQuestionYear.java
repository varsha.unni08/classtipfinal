package com.classtips.liju.classtips.Database.ModelDB;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by User on 9/27/2018.
 */
@Entity(tableName ="classtipQuestionYear" )
public class classtipQuestionYear {

    @NonNull
    @PrimaryKey
    @ColumnInfo(name="id")
    public String id;

    @ColumnInfo(name="examId")
    public String examId;

    @ColumnInfo(name="question")
    public String question;

    @ColumnInfo(name="questionVideo")
    public String questionVideo;

    @ColumnInfo(name="topicId")
    public String topicId;

    @ColumnInfo(name="questionType")
    public String questionType;

    @ColumnInfo(name="groupNo")
    public String groupNo;

    @ColumnInfo(name="orderNo")
    public String orderNo;

    @ColumnInfo(name="answerImage")
    public String answerImage;


}
