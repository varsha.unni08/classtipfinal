package com.classtips.liju.classtips.Adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.R;

/**
 * Created by User on 8/30/2018.
 */

public class PurchaseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener  {
    ImageView image_purchase;
    TextView txt_purchase,text_price;
    Button btn_purchase;
    public CardView cardView;

    IItemClickListner itemClickListner;

    public void setItemClickListner(IItemClickListner itemClickListner) {
        this.itemClickListner = itemClickListner;
    }

    public PurchaseViewHolder(View itemView) {
        super(itemView);
        image_purchase=(ImageView)itemView.findViewById(R.id.image_purchase);
        txt_purchase=(TextView)itemView.findViewById(R.id.txt_purchase);
        text_price=(TextView)itemView.findViewById(R.id.text_price);
        btn_purchase=(Button)itemView.findViewById(R.id.btn_purchase);
        cardView=(CardView)itemView.findViewById(R.id.card_view);

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
    itemClickListner.onClick(v);
    }
}
