package com.classtips.liju.classtips.Database.AdapterDB;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.classtips.liju.classtips.Adapter.ChapterViewHolder;
import com.classtips.liju.classtips.Database.ModelDB.classtipChapter;
import com.classtips.liju.classtips.Database.ModelDB.classtipSubject;
import com.classtips.liju.classtips.Interface.IItemClickListner;
import com.classtips.liju.classtips.R;
import com.classtips.liju.classtips.TopicActivity;
import com.classtips.liju.classtips.Utils.Common;

import java.util.List;

/**
 * Created by User on 9/8/2018.
 */

public class ChapterAdapterDB extends RecyclerView.Adapter<ChapterViewHolder> {
    Context context;
    List<classtipChapter> chapters;

    public ChapterAdapterDB(Context context, List<classtipChapter> chapters) {
        this.context = context;
        this.chapters = chapters;
    }

    @Override
    public ChapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(context).inflate(R.layout.chapter_item_layout,null);
        return new ChapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ChapterViewHolder holder, final int position) {
    holder.txt_chapter_name.setText(chapters.get(position).chapter_name);

    //Common.currentClasstipChapter=chapters.get(position);

    holder.setItemClickListner(new IItemClickListner() {
        @Override
        public void onClick(View v) {
            Common.currentClasstipChapter=chapters.get(position);
            Common.currentChapter=null;
            //Common.chapterRepository= chapters.get(position);

                context.startActivity(new Intent(context,TopicActivity.class));


        }
    });
    }

    @Override
    public int getItemCount() {
        return chapters.size();
    }

    public void setChaptertList(List<classtipChapter> classtipChapters) {
        this.chapters = classtipChapters;
        notifyDataSetChanged();
    }
}
